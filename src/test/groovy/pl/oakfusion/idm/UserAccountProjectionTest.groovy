package pl.oakfusion.idm

import pl.oakfusion.idm.user.AccountState
import pl.oakfusion.idm.user.activation.AccountActivated
import pl.oakfusion.idm.user.authentication.Authenticated
import pl.oakfusion.idm.user.authentication.AuthenticationFailed
import pl.oakfusion.idm.user.registration.AccountRegisteredWithPassword
import pl.oakfusion.roots.api.mail.Email
import pl.oakfusion.roots.api.messagebus.MessageBus
import pl.oakfusion.roots.api.readmodel.ReadModelStorage
import pl.oakfusion.roots.api.version.Version
import spock.lang.Specification
import spock.lang.Subject

class UserAccountProjectionTest extends Specification {

	private static def READ_MODEL_NAME = "user-read-model"

	private ReadModelStorage modelStorage = Mock()

	private MessageBus messageBus = Stub()

	@Subject
	private UserAccountProjection accountProjection

	private static AccountRegisteredWithPassword accountRegistered(UUID aggregateId, String emailAddress, String passwordHash, Email activationEmail) {
		new AccountRegisteredWithPassword(aggregateId, null, emailAddress, passwordHash, activationEmail)
	}

	private static AccountActivated accountActivated(UUID aggregateId) {
		new AccountActivated(null, aggregateId, null)
	}

	private static Authenticated authenticated(UUID aggregateId) {
		def authenticated = new Authenticated()
		authenticated.setAggregateId(aggregateId)
		authenticated.setCreatorId(aggregateId)
		authenticated
	}

	private static AuthenticationFailed authenticationFailed(UUID aggregateId) {
		new AuthenticationFailed(aggregateId, aggregateId, null)
	}

	def setup() {
		modelStorage.get(READ_MODEL_NAME, _) >> []
		accountProjection = new UserAccountProjection(READ_MODEL_NAME, { new User() }, modelStorage, messageBus)
	}

	def "should mark user as registered"() {
		given:
			def aggregateId = UUID.randomUUID()
			def emailAddress = "mail@test.com"
			def passwordHash = "abcde"
			def activationEmail = new Email()
			def event = accountRegistered(aggregateId, emailAddress, passwordHash, activationEmail)
			event.version = Version.of(1)

			User expectedUser = new User()
			expectedUser.name = emailAddress
			expectedUser.aggregateId = aggregateId
			expectedUser.state = AccountState.INACTIVE
			expectedUser.creationTimestamp = event.creationTimestamp
			expectedUser.registrationTimestamp = event.creationTimestamp
			expectedUser.version = event.version
		when:
			accountProjection.handle(event)
		then:
			1 * modelStorage.store(READ_MODEL_NAME, expectedUser, event.version)
	}

	def "should mark user as activated"() {
		given:
			def aggregateId = UUID.randomUUID()
			def event = accountActivated(aggregateId)
			event.version = Version.of(4)

			User expectedUser = new User()
			expectedUser.aggregateId = aggregateId
			expectedUser.state = AccountState.ACTIVE
			expectedUser.version = event.version
			expectedUser.activationTimestamp = event.creationTimestamp

		when:
			accountProjection.handle(event)
		then:
			1 * modelStorage.store(READ_MODEL_NAME, expectedUser, event.version)
	}

	def "should set last successful login timestamp"() {
		given:
			def aggregateId = UUID.randomUUID()
			def event = authenticated(aggregateId)
			event.version = Version.of(4)

			User expectedUser = new User()
			expectedUser.aggregateId = aggregateId
			expectedUser.version = event.version
			expectedUser.lastSuccessfulLoginTimestamp = event.creationTimestamp

		when:
			accountProjection.handle(event)
		then:
			1 * modelStorage.store(READ_MODEL_NAME, expectedUser, event.version)
	}

	def "should update last successful login timestamp"() {
		given:
			def aggregateId = UUID.randomUUID()
			def firstEvent = authenticated(aggregateId)
			firstEvent.version = Version.of(4)

			def secondEvent = authenticated(aggregateId)
			secondEvent.version = Version.of(5)


			User firstUser = new User()
			firstUser.aggregateId = aggregateId
			firstUser.version = firstEvent.version
			firstUser.lastSuccessfulLoginTimestamp = firstEvent.creationTimestamp

			User secondUser = new User()
			secondUser.aggregateId = aggregateId
			secondUser.version = firstEvent.version
			secondUser.lastSuccessfulLoginTimestamp = secondEvent.creationTimestamp

		when:
			accountProjection.handle(firstEvent)
			accountProjection.handle(secondEvent)
		then:
			1 * modelStorage.store(READ_MODEL_NAME, firstUser, firstEvent.version)
			1 * modelStorage.store(READ_MODEL_NAME, secondUser, secondEvent.version)
	}

	def "should set last failed login timestamp"() {
		given:
			def aggregateId = UUID.randomUUID()
			def event = authenticationFailed(aggregateId)
			event.version = Version.of(4)

			User expectedUser = new User()
			expectedUser.aggregateId = aggregateId
			expectedUser.version = event.version
			expectedUser.lastFailedLoginTimestamp = event.creationTimestamp

		when:
			accountProjection.handle(event)
		then:
			1 * modelStorage.store(READ_MODEL_NAME, expectedUser, event.version)
	}

	def "should update last failed login timestamp"() {
		given:
			def aggregateId = UUID.randomUUID()
			def firstEvent = authenticationFailed(aggregateId)
			firstEvent.version = Version.of(4)

			def secondEvent = authenticationFailed(aggregateId)
			secondEvent.version = Version.of(5)


			User firstUser = new User()
			firstUser.aggregateId = aggregateId
			firstUser.version = firstEvent.version
			firstUser.lastFailedLoginTimestamp = firstEvent.creationTimestamp

			User secondUser = new User()
			secondUser.aggregateId = aggregateId
			secondUser.version = firstEvent.version
			secondUser.lastFailedLoginTimestamp = secondEvent.creationTimestamp

		when:
			accountProjection.handle(firstEvent)
			accountProjection.handle(secondEvent)
		then:
			1 * modelStorage.store(READ_MODEL_NAME, firstUser, firstEvent.version)
			1 * modelStorage.store(READ_MODEL_NAME, secondUser, secondEvent.version)
	}
}
