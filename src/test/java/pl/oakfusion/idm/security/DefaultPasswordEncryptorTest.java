package pl.oakfusion.idm.security;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class DefaultPasswordEncryptorTest {

	private static final String PASSWORD = "pa8sW0rDd_*";
	private DefaultPasswordEncryptor passwordEncryptor = new DefaultPasswordEncryptor();

	@Test
	public void should_encrypt_password() {
		// when
		String hash1 = passwordEncryptor.encryptPassword(PASSWORD);
		String hash2 = passwordEncryptor.encryptPassword(PASSWORD);

		// then
		assertThat(hash1).isNotEqualTo(PASSWORD);
		assertThat(hash2).isNotEqualTo(PASSWORD);
		assertThat(hash1).isNotEqualTo(hash2);
	}

	@Test
	public void should_check_password() {
		// given
		String hash = passwordEncryptor.encryptPassword(PASSWORD);

		// then
		assertThat(passwordEncryptor.checkPassword(PASSWORD, hash)).isTrue();
		assertThat(passwordEncryptor.checkPassword("some-random-string", hash)).isFalse();
	}

}