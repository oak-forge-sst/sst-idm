package pl.oakfusion.idm.web;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.oakfusion.idm.Config;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {
		IdmApplication.class,
		Config.class
})
public class IdmApplicationTests {

	@Test
	@Ignore
	public void contextLoads() {
	}

}
