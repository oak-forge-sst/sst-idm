package pl.oakfusion.idm.user;

import org.junit.Test;
import pl.oakfusion.idm.exception.AccountRegistrationException;
import pl.oakfusion.idm.user.registration.AccountRegisteredWithPassword;
import pl.oakfusion.idm.user.registration.RegisterAccountWithPassword;
import pl.oakfusion.roots.api.mail.Email;

import java.util.UUID;

import static java.util.UUID.randomUUID;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Maps.newHashMap;
import static pl.oakfusion.idm.exception.ErrorCode.ACTIVATION_MESSAGE_FAILURE;
import static pl.oakfusion.idm.exception.ErrorCode.EMAIL_ALREADY_TAKEN;
import static pl.oakfusion.idm.exception.ErrorCode.INVALID_EMAIL_ADDRESS_FORMAT;
import static pl.oakfusion.idm.exception.ErrorCode.PASSWORD_AND_PASSWORD_CONFIRMATION_ARE_BOTH_REQUIRED;
import static pl.oakfusion.idm.exception.ErrorCode.PASSWORD_AND_PASSWORD_CONFIRMATION_DO_NOT_MATCH;
import static pl.oakfusion.idm.user.registration.Register.ACTIVATION_MESSAGE_NAME;
import static pl.oakfusion.idm.user.registration.Register.ACTIVATION_TOKEN;
import static pl.oakfusion.idm.user.registration.Register.USER;
import static pl.oakfusion.roots.testutils.AggregateTestUtils.v;
import static pl.oakfusion.roots.testutils.TestEmailManager.DEFAULT_SENDER_ADDRESS;

public class RegistrationTest extends UserAggregateTestBase {

	private static final String INVALID_EMAIL_ADDRESS = "invalid-email";

	private RegisterAccountWithPassword registerAccount() {
		return new RegisterAccountWithPassword(EMAIL_ADDRESS, PASSWORD, PASSWORD_CONFIRMATION);
	}

	@Test
	public void should_not_register_account_if_email_already_taken_for_active_account() {
		fixture.given(this::initEmailLookupRandomly)
			   .when(registerAccount())
			   .thenExpectException(AccountRegistrationException.class)
			   .withMessageContaining(EMAIL_ALREADY_TAKEN.name())
		;
	}

	@Test
	public void should_fail_registering_with_invalid_email_address_format() {
		fixture.given()
			   .when(new RegisterAccountWithPassword(INVALID_EMAIL_ADDRESS, PASSWORD, PASSWORD_CONFIRMATION))
			   .thenExpectException(AccountRegistrationException.class)
			   .withMessageContaining(INVALID_EMAIL_ADDRESS_FORMAT.name())
		;
	}

	@Test
	public void should_fail_registering_when_password_is_missing() {
		fixture.given()
			   .when(new RegisterAccountWithPassword(EMAIL_ADDRESS, EMPTY_PASSWORD, PASSWORD_CONFIRMATION))
			   .thenExpectException(AccountRegistrationException.class)
			   .withMessageContaining(PASSWORD_AND_PASSWORD_CONFIRMATION_ARE_BOTH_REQUIRED.name())
		;
	}

	@Test
	public void should_fail_registering_when__password_confirmation_is_missing() {
		fixture.given()
			   .when(new RegisterAccountWithPassword(EMAIL_ADDRESS, PASSWORD, EMPTY_PASSWORD))
			   .thenExpectException(AccountRegistrationException.class)
			   .withMessageContaining(PASSWORD_AND_PASSWORD_CONFIRMATION_ARE_BOTH_REQUIRED.name())
		;
	}


	@Test
	public void should_fail_registering_when_password_and_password_confirmation_do_not_match() {
		fixture.given()
			   .when(new RegisterAccountWithPassword(EMAIL_ADDRESS, PASSWORD, WRONG_PASSWORD_CONFIRMATION))
			   .thenExpectException(AccountRegistrationException.class)
			   .withMessageContaining(PASSWORD_AND_PASSWORD_CONFIRMATION_DO_NOT_MATCH.name())
		;
	}

	@Test
	public void should_fail_registering_already_activated_account() {
		fixture.given(accountRegistered(), accountActivated())
			   .andMocks(this::initEmailLookupCorrectly)
			   .when(registerAccount())
			   .thenExpectException(AccountRegistrationException.class)
			   .withMessageContaining(EMAIL_ALREADY_TAKEN.name())
		;
	}

	@Test
	public void should_fail_during_activation_email_preparation() {
		fixture.given(this::initEmptyEmailLookup)
			   .andMocks(() -> manager.nextFailure(new RuntimeException("cannot prepare")))
			   .when(registerAccount())
			   .thenExpectException(AccountRegistrationException.class)
			   .withMessageContaining(ACTIVATION_MESSAGE_FAILURE.name())
		;
	}

	@Test
	public void should_register_account() {
		fixture.given(this::initEmptyEmailLookup)
			   .andMocks(this::initEncryptionHashing)
			   .when(registerAccount())
			   .thenExpectEvent(AccountRegisteredWithPassword.class)
			   .matching(event -> {
				   AccountRegisteredWithPassword.Data data = event.getPayload();
				   Email email = data.getActivationEmailMessage();
				   assertThat(data.getEmailAddress()).isEqualTo(EMAIL_ADDRESS);
				   assertThat(email.getId()).isNotNull();
				   assertThat(email.getTemplateName()).isEqualTo(ACTIVATION_MESSAGE_NAME);
				   assertThat(email.getFrom()).isEqualTo(DEFAULT_SENDER_ADDRESS);
				   assertThat(email.getTo()).contains(EMAIL_ADDRESS);
				   assertThat(email.getSubject()).contains(ACTIVATION_MESSAGE_NAME);
				   assertThat(email.getParams()).containsKey(ACTIVATION_TOKEN);
				   assertThat(email.getParams()).containsKey(USER);
			   })
			   .thenExpectNoEvents()
		;
	}

	@Test
	public void should_register_account_with_different_creator_id() {
		UUID creatorId = randomUUID();
		RegisterAccountWithPassword command = registerAccount();
		command.setCreatorId(creatorId);

		fixture.given(this::initEmptyEmailLookup)
			   .when(command)
			   .thenExpectEvent(AccountRegisteredWithPassword.class)
			   .matching(event -> {
				   assertThat(event.getCreatorId()).isEqualTo(creatorId);
			   })
			   .thenExpectNoEvents()
		;
	}

	@Test
	public void should_init_state_after_registration() {
		Email email = new Email();
		email.setTo(new String[]{EMAIL_ADDRESS});
		email.setParams(newHashMap(ACTIVATION_TOKEN, AGGREGATE_ID));

		fixture.given(AGGREGATE_ID)
			   .andEvents(accountRegistered(email))
			   .when()
			   .thenExpectNoEvents()
			   .andStateMatching(account -> {
				   assertThat(account.getVersion()).isEqualTo(v(1));
				   assertThat(account.getAggregateId()).isEqualTo(AGGREGATE_ID);
				   assertThat(account.emailAddress).isEqualTo(EMAIL_ADDRESS);
				   assertThat(account.activationTimestamp).isNull();
				   assertThat(account.isActive()).isFalse();
			   })
		;
	}

}
