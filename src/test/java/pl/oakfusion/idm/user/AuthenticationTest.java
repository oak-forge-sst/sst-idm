package pl.oakfusion.idm.user;

import org.junit.Test;
import pl.oakfusion.idm.security.AuthenticationException;
import pl.oakfusion.idm.user.activation.AccountActivated;
import pl.oakfusion.idm.user.authentication.AccountBlocked;
import pl.oakfusion.idm.user.authentication.Authenticate;
import pl.oakfusion.idm.user.authentication.Authenticated;
import pl.oakfusion.idm.user.authentication.AuthenticationFailed;
import pl.oakfusion.idm.user.expiration.AccountExpired;
import pl.oakfusion.idm.user.expiration.ExpirationDuration;
import pl.oakfusion.roots.api.event.Event;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.time.Instant.now;
import static org.assertj.core.api.Assertions.assertThat;
import static pl.oakfusion.idm.exception.ErrorCode.ACCOUNT_BLOCKED;
import static pl.oakfusion.idm.exception.ErrorCode.ACCOUNT_EXPIRED;
import static pl.oakfusion.idm.exception.ErrorCode.EMAIL_AND_PASSWORD_ARE_BOTH_REQUIRED;
import static pl.oakfusion.idm.exception.ErrorCode.INCORRECT_LOGIN_OR_PASSWORD;

public class AuthenticationTest extends UserAggregateTestBase {

	private Authenticate correctAuthenticate() {
		return new Authenticate(EMAIL_ADDRESS, PASSWORD);
	}

	private Authenticate invalidAuthenticate() {
		return new Authenticate(EMAIL_ADDRESS, EMPTY_PASSWORD);
	}

	private Authenticate wrongPasswordAuthenticate() {
		return new Authenticate(EMAIL_ADDRESS, WRONG_PASSWORD);
	}

	private List<Event> failedAuthentications(int times) {
		return IntStream.range(0, times).mapToObj(x -> new AuthenticationFailed(INCORRECT_LOGIN_OR_PASSWORD)).collect(Collectors.toList());
	}

	@Test
	public void should_authenticate_with_valid_credentials() {
		fixture.given(accountRegistered(), accountActivated())
			   .andMocks(this::initEncryptionHashing)
			   .andMocks(this::initCorrectPasswordEncryptionCheck)
			   .andMocks(this::initEmailLookupCorrectly)
			   .when(correctAuthenticate())
			   .thenExpectEvent(Authenticated.class)
			   .matching(this::checkAggregateId)
			   .thenExpectNoEvents()
		;
	}

	@Test
	public void should_throw_during_preparation_of_authentication_command_for_non_registered_email() {
		fixture.given()
			   .andMocks(this::initEmptyEmailLookup)
			   .when(correctAuthenticate())
			   .thenExpectException(AuthenticationException.class)
			   .withMessageContaining(INCORRECT_LOGIN_OR_PASSWORD.name())
		;
	}

	@Test
	public void should_fail_authentication_with_invalid_password() {
		fixture.given()
			   .andMocks(this::initEmailLookupCorrectly)
			   .when(invalidAuthenticate())
			   .thenExpectException(AuthenticationException.class)
			   .withMessageContaining(EMAIL_AND_PASSWORD_ARE_BOTH_REQUIRED.name())
		;
	}

	@Test
	public void should_fail_authentication_for_expired_account() {
		fixture.given()
			   .andEvents(accountRegistered(), accountActivated(), new AccountExpired())
			   .andMocks(this::initEmailLookupCorrectly)
			   .andMocks(this::initWrongPasswordEncryptionCheck)
			   .when(correctAuthenticate())
			   .thenExpectEvent(AuthenticationFailed.class)
			   .matching(event -> {
				   assertThat(event.getPayload()).isEqualTo(ACCOUNT_EXPIRED);
			   })
			   .thenExpectNoEvents()
		;
	}

	@Test
	public void should_fail_authentication_for_blocked_account() {
		fixture.given()
			   .andEvents(accountRegistered(), accountActivated(), new AccountBlocked())
			   .andMocks(this::initEmailLookupCorrectly)
			   .andMocks(this::initWrongPasswordEncryptionCheck)
			   .when(correctAuthenticate())
			   .thenExpectEvent(AuthenticationFailed.class)
			   .matching(event -> {
				   assertThat(event.getPayload()).isEqualTo(ACCOUNT_BLOCKED);
			   })
			   .thenExpectNoEvents()
		;
	}

	@Test
	public void should_fail_authentication_for_inactive_account() {
		fixture.given(accountRegistered(), accountActivated())
			   .andMocks(this::initEmptyEmailLookup)
			   .when(correctAuthenticate())
			   .thenExpectException(AuthenticationException.class)
			   .withMessageContaining(INCORRECT_LOGIN_OR_PASSWORD.name())
		;
	}

	@Test
	public void should_fail_authentication_for_wrong_password() {
		fixture.given(accountRegistered(), accountActivated())
			   .andMocks(this::initEncryptionHashing)
			   .andMocks(this::initEmailLookupCorrectly)
			   .when(wrongPasswordAuthenticate())
			   .thenExpectEvent(AuthenticationFailed.class)
			   .matching(event -> {
				   assertThat(event.getPayload()).isEqualTo(INCORRECT_LOGIN_OR_PASSWORD);
			   })
			   .matching(this::checkAggregateId)
			   .thenExpectNoEvents()
		;
	}

	@Test
	public void should_block_account_when_exceeded_failed_authentication_threshold_on_issuing_command() {
		fixture.given(AGGREGATE_ID)
			   .andEvents(accountRegistered(), accountActivated())
			   .andEvents(failedAuthentications(FAILED_AUTHENTICATION_THRESHOLD))
			   .andMocks(this::initEmailLookupCorrectly)
			   .andMocks(this::initWrongPasswordEncryptionCheck)
			   .when(wrongPasswordAuthenticate())
			   .thenExpectEvent(AuthenticationFailed.class)
			   .matching(event -> {
				   assertThat(event.getPayload()).isEqualTo(ACCOUNT_BLOCKED);
			   })
			   .thenExpectEvent(AccountBlocked.class)
			   .thenExpectNoEvents()
			   .andStateMatching(account -> {
				   assertThat(account.isActive()).isFalse();
			   })
		;
	}


	@Test
	public void should_not_block_account_and_reset_failure_count_after_successful_authentication_within_failure_threshold() {
		Instant before = now();

		fixture.given()
			   .andEvents(accountRegistered(), accountActivated())
			   .andEvents(failedAuthentications(FAILED_AUTHENTICATION_THRESHOLD - 1))
			   .andMocks(this::initEmailLookupCorrectly)
			   .andMocks(this::initCorrectPasswordEncryptionCheck)
			   .andMocks(this::initWrongPasswordEncryptionCheck)
			   .when(correctAuthenticate())
			   .thenExpectEvent(Authenticated.class)
			   .thenExpectNoEvents()
			   .andStateMatching(account -> {
				   assertThat(account.isActive()).isTrue();
				   assertThat(account.lastSuccessfulAuthenticationTimestamp).isBetween(before, now());
				   assertThat(account.authenticationFailureCount).isEqualTo(0);
			   })
		;
	}

	@Test
	public void should_mark_account_as_expired_when_issuing_authentication_command_after_expiration_timestamp() {
		ExpirationDuration beforeNowTimestamp = ExpirationDuration.ofDuration(Duration.ofSeconds(-10));

		fixture.given(AGGREGATE_ID)
			   .andEvents(accountRegistered(), new AccountActivated(beforeNowTimestamp, AGGREGATE_ID, AGGREGATE_ID))
			   .andMocks(this::initEmailLookupCorrectly)
			   .when(correctAuthenticate())
			   .thenExpectEvent(AuthenticationFailed.class)
			   .matching(event -> {
				   assertThat(event.getPayload()).isEqualTo(ACCOUNT_EXPIRED);
			   })
			   .thenExpectEvent(AccountExpired.class)
			   .thenExpectNoEvents()
			   .andStateMatching(account -> {
				   assertThat(account.isActive()).isFalse();
			   })
		;
	}

}
