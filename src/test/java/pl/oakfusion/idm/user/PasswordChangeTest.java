package pl.oakfusion.idm.user;

import org.junit.Test;
import pl.oakfusion.idm.user.activation.AccountActivated;
import pl.oakfusion.idm.user.authentication.AccountBlocked;
import pl.oakfusion.idm.user.expiration.AccountExpired;
import pl.oakfusion.idm.user.expiration.ExpirationDuration;
import pl.oakfusion.idm.user.password.change.ChangePassword;
import pl.oakfusion.idm.user.password.change.PasswordChangeException;
import pl.oakfusion.idm.user.password.change.PasswordChangeFailed;
import pl.oakfusion.idm.user.password.change.PasswordChanged;

import java.time.Duration;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static pl.oakfusion.idm.exception.ErrorCode.ACCOUNT_BLOCKED;
import static pl.oakfusion.idm.exception.ErrorCode.ACCOUNT_EXPIRED;
import static pl.oakfusion.idm.exception.ErrorCode.INCORRECT_LOGIN_OR_PASSWORD;
import static pl.oakfusion.idm.exception.ErrorCode.PASSWORD_AND_PASSWORD_CONFIRMATION_ARE_BOTH_REQUIRED;
import static pl.oakfusion.idm.exception.ErrorCode.PASSWORD_AND_PASSWORD_CONFIRMATION_DO_NOT_MATCH;
import static pl.oakfusion.idm.user.AccountState.EXPIRED;

public class PasswordChangeTest extends UserAggregateTestBase {

	private static final String CHANGED_PASSWORD = "changed-password";
	private static final String CHANGED_PASSWORD_HASH = "changed-password-hashed";
	private static final String CHANGED_PASSWORD_CONFIRMATION = CHANGED_PASSWORD;

	private ChangePassword correctPasswordChange() {
		return new ChangePassword(EMAIL_ADDRESS, PASSWORD, CHANGED_PASSWORD, CHANGED_PASSWORD_CONFIRMATION);
	}

	private ChangePassword wrongPasswordChange() {
		return new ChangePassword(EMAIL_ADDRESS, PASSWORD, WRONG_PASSWORD, WRONG_PASSWORD_CONFIRMATION);
	}

	private void initChangedPasswordEncryptionHashing() {
		given(encryptor.encryptPassword(CHANGED_PASSWORD)).willReturn(CHANGED_PASSWORD_HASH);
	}

	@Test
	public void should_change_password() {
		fixture.given(accountRegistered(), accountActivated())
			   .andMocks(this::initEmailLookupCorrectly)
			   .andMocks(this::initEncryptionHashing)
			   .andMocks(this::initChangedPasswordEncryptionHashing)
			   .andMocks(this::initCorrectPasswordEncryptionCheck)
			   .when(correctPasswordChange())
			   .thenExpectEvent(PasswordChanged.class)
			   .matching(event -> {
				   assertThat(event.getPayload()).isEqualTo(CHANGED_PASSWORD_HASH);
			   })
			   .thenExpectNoEvents()
			   .andStateMatching(account -> {
				   assertThat(account.passwordHash).isEqualTo(CHANGED_PASSWORD_HASH);
			   })
		;
	}

	@Test
	public void should_fail_password_change_for_inactive_account() {
		fixture.given(accountRegistered(), accountActivated())
			   .andMocks(this::initEmptyEmailLookup)
			   .when(correctPasswordChange())
			   .thenExpectException(PasswordChangeException.class)
			   .withMessageContaining(INCORRECT_LOGIN_OR_PASSWORD.name())
		;
	}

	@Test
	public void should_fail_password_change_when_new_password_is_missing() {
		fixture.given(accountRegistered(), accountActivated())
			   .andMocks(this::initEmailLookupCorrectly)
			   .when(new ChangePassword(EMAIL_ADDRESS, PASSWORD, EMPTY_PASSWORD, CHANGED_PASSWORD_CONFIRMATION))
			   .thenExpectException(PasswordChangeException.class)
			   .withMessageContaining(PASSWORD_AND_PASSWORD_CONFIRMATION_ARE_BOTH_REQUIRED.name())
		;
	}

	@Test
	public void should_fail_password_change_when_new_password_confirmation_is_missing() {
		fixture.given(accountRegistered(), accountActivated())
			   .andMocks(this::initEmailLookupCorrectly)
			   .when(new ChangePassword(EMAIL_ADDRESS, PASSWORD, CHANGED_PASSWORD, EMPTY_PASSWORD))
			   .thenExpectException(PasswordChangeException.class)
			   .withMessageContaining(PASSWORD_AND_PASSWORD_CONFIRMATION_ARE_BOTH_REQUIRED.name())
		;
	}

	@Test
	public void should_fail_password_change_when_new_password_and_new_password_confirmation_do_not_match() {
		fixture.given(accountRegistered(), accountActivated())
			   .andMocks(this::initEmailLookupCorrectly)
			   .when(new ChangePassword(EMAIL_ADDRESS, PASSWORD, CHANGED_PASSWORD, WRONG_PASSWORD_CONFIRMATION))
			   .thenExpectException(PasswordChangeException.class)
			   .withMessageContaining(PASSWORD_AND_PASSWORD_CONFIRMATION_DO_NOT_MATCH.name())
		;
	}

	@Test
	public void should_fail_password_change_for_expired_account() {
		fixture.given(accountRegistered(), accountActivated(), new AccountExpired())
			   .andMocks(this::initEmailLookupCorrectly)
			   .andMocks(this::initCorrectPasswordEncryptionCheck)
			   .when(correctPasswordChange())
			   .thenExpectEvent(PasswordChangeFailed.class)
			   .matching(event -> {
				   assertThat(event.getPayload()).isEqualTo(ACCOUNT_EXPIRED);
			   })
			   .thenExpectNoEvents()
		;
	}

	@Test
	public void should_fail_password_change_for_blocked_account() {
		fixture.given(accountRegistered(), accountActivated(), new AccountBlocked())
			   .andMocks(this::initEmailLookupCorrectly)
			   .andMocks(this::initCorrectPasswordEncryptionCheck)
			   .when(correctPasswordChange())
			   .thenExpectEvent(PasswordChangeFailed.class)
			   .matching(event -> {
				   assertThat(event.getPayload()).isEqualTo(ACCOUNT_BLOCKED);
			   })
			   .thenExpectNoEvents()
		;
	}

	@Test
	public void should_fail_password_change_with_wrong_password() {
		fixture.given(accountRegistered(), accountActivated())
			   .andMocks(this::initEmailLookupCorrectly)
			   .andMocks(this::initWrongPasswordEncryptionCheck)
			   .when(wrongPasswordChange())
			   .thenExpectEvent(PasswordChangeFailed.class)
			   .matching(event -> {
				   assertThat(event.getPayload()).isEqualTo(INCORRECT_LOGIN_OR_PASSWORD);
			   })
			   .thenExpectNoEvents()
		;
	}

	@Test
	public void should_expire_account_when_sending_command_after_expiration_timestamp() {
		ExpirationDuration beforeNowTimestamp = ExpirationDuration.ofDuration(Duration.ofSeconds(-10));

		fixture.given(AGGREGATE_ID)
			   .andEvents(accountRegistered(), new AccountActivated(beforeNowTimestamp, AGGREGATE_ID, AGGREGATE_ID))
			   .andMocks(this::initEmailLookupCorrectly)
			   .when(correctPasswordChange())
			   .thenExpectEvent(PasswordChangeFailed.class)
			   .matching(event -> {
				   assertThat(event.getPayload()).isEqualTo(ACCOUNT_EXPIRED);
			   })
			   .thenExpectEvent(AccountExpired.class)
			   .thenExpectNoEvents()
			   .andStateMatching(account -> {
				   assertThat(account.state).isEqualTo(EXPIRED);
			   })
		;
	}

}
