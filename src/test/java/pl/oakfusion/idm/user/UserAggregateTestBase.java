package pl.oakfusion.idm.user;

import org.mockito.Matchers;
import pl.oakfusion.idm.EmailLookup;
import pl.oakfusion.idm.security.PasswordEncryptor;
import pl.oakfusion.idm.security.SecurityConstants;
import pl.oakfusion.idm.user.activation.AccountActivated;
import pl.oakfusion.idm.user.expiration.AccountExpirationStrategy;
import pl.oakfusion.idm.user.expiration.ExpirationDuration;
import pl.oakfusion.idm.user.expiration.NoExpirationStrategy;
import pl.oakfusion.idm.user.registration.AccountRegisteredWithPassword;
import pl.oakfusion.idm_commands.Roles;
import pl.oakfusion.roots.api.event.Event;
import pl.oakfusion.roots.api.mail.Email;
import pl.oakfusion.roots.api.test.configuration.TestConfiguration;
import pl.oakfusion.roots.testutils.TestEmailManager;
import pl.oakfusion.idm.security.BlacklistJWTLookup;
import pl.oakfusion.security.TokenManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;
import java.util.UUID;

import static java.time.Instant.now;
import static java.util.UUID.randomUUID;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static pl.oakfusion.roots.impl.test.AggregateFixtureFactory.fixture;

abstract class UserAggregateTestBase {

	static final UUID AGGREGATE_ID = randomUUID();

	static final String EMAIL_ADDRESS = "email@example.com";

	static final String PASSWORD = "password";
	static final String PASSWORD_HASH = "password-hashed";
	static final String PASSWORD_CONFIRMATION = PASSWORD;

	static final String EMPTY_PASSWORD = "";

	static final String WRONG_PASSWORD = "wrong-password";
	static final String WRONG_PASSWORD_CONFIRMATION = WRONG_PASSWORD;

	static final String TEST_KEY = "!A%D*G-KaPdSgVkYp3s6v8y/B?E(H+MbQeThWmZq4t7w!z$C&F)J@NcRfUjXn2r5";


	static final int ACTIVATION_MESSAGE_EXPIRATION_PERIOD_ID_DAYS = 1;
	static final int FAILED_AUTHENTICATION_THRESHOLD = 3;

	final EmailLookup lookup = mock(EmailLookup.class);
	final AccountExpirationStrategy expirationStrategy = new NoExpirationStrategy();
	final TestEmailManager manager = new TestEmailManager();
	final PasswordEncryptor encryptor = mock(PasswordEncryptor.class);
	final BlacklistJWTLookup blacklistJWTLookup = mock(BlacklistJWTLookup.class);
	final TokenManager tokenManager = mock(TokenManager.class);

	final TestConfiguration<UserAggregate.Account> fixture = fixture(this::createAggregate);

	private UserAggregate createAggregate() {
		SecurityConstants securityConstants = new SecurityConstants();
		securityConstants.setKey(TEST_KEY);
		when(encryptor.encryptPassword(Matchers.anyString())).thenReturn("passwordHash");
		return new UserAggregate(
				ACTIVATION_MESSAGE_EXPIRATION_PERIOD_ID_DAYS,
				FAILED_AUTHENTICATION_THRESHOLD,
				lookup,
				expirationStrategy,
				manager,
				encryptor,
				securityConstants,
				new Roles(),
				blacklistJWTLookup,
				tokenManager
		);
	}

	AccountRegisteredWithPassword accountRegistered(Email email) {
		return new AccountRegisteredWithPassword(AGGREGATE_ID, AGGREGATE_ID, EMAIL_ADDRESS, PASSWORD_HASH, email, new ArrayList<>(Collections.singletonList(new Roles().getDefaultRole())));
	}

	AccountRegisteredWithPassword accountRegistered() {
		return accountRegistered(new Email());
	}

	AccountActivated accountActivated() {
		return new AccountActivated(ExpirationDuration.noExpiration());
	}

	void initEmailLookupCorrectly() {
		given(lookup.get(EMAIL_ADDRESS)).willReturn(Optional.of(AGGREGATE_ID));
	}

	void initEmailLookupRandomly() {
		given(lookup.get(EMAIL_ADDRESS)).willReturn(Optional.of(randomUUID()));
	}

	void initEmptyEmailLookup() {
		given(lookup.get(EMAIL_ADDRESS)).willReturn(Optional.empty());
	}

	void initEncryptionHashing() {
		given(encryptor.encryptPassword(PASSWORD)).willReturn(PASSWORD_HASH);
	}

	void initCorrectPasswordEncryptionCheck() {
		given(encryptor.checkPassword(PASSWORD, PASSWORD_HASH)).willReturn(true);
	}

	void initWrongPasswordEncryptionCheck() {
		given(encryptor.checkPassword(WRONG_PASSWORD, PASSWORD_HASH)).willReturn(false);
	}

	void prepareStateForActivation(UserAggregate.Account account) {
		initEmailAddress(account);
		account.activationMessageTimestamp = now();
	}

	void initEmailAddress(UserAggregate.Account account) {
		account.emailAddress = EMAIL_ADDRESS;
	}

	void checkAggregateId(Event event) {
		assertThat(event.getAggregateId()).isEqualTo(AGGREGATE_ID);
		assertThat(event.getCreatorId()).isEqualTo(AGGREGATE_ID);
	}

}
