package pl.oakfusion.idm.user;

import org.junit.Test;
import pl.oakfusion.idm.exception.AccountRegistrationException;
import pl.oakfusion.idm.user.activation.AccountActivated;
import pl.oakfusion.idm.user.activation.ActivateAccount;
import pl.oakfusion.idm.user.activation.ActivateAccountWithPassword;
import pl.oakfusion.idm.user.expiration.ExpirationDuration;

import java.time.temporal.ChronoUnit;

import static java.time.Instant.now;
import static java.util.UUID.randomUUID;
import static org.assertj.core.api.Assertions.assertThat;
import static pl.oakfusion.idm.exception.ErrorCode.ACTIVATION_TOKEN_EXPIRED;
import static pl.oakfusion.idm.exception.ErrorCode.ACTIVATION_TOKEN_INVALID;
import static pl.oakfusion.idm.exception.ErrorCode.EMAIL_ALREADY_TAKEN;

public class ActivationTest extends UserAggregateTestBase {

	private ActivateAccount activateAccount() {
		return new ActivateAccount(AGGREGATE_ID);
	}

	private ActivateAccountWithPassword activateAccountWithPassword() {
		return new ActivateAccountWithPassword(AGGREGATE_ID, PASSWORD, PASSWORD);
	}

	@Test
	public void should_activate_account_if_not_active_yet() {
		fixture.given(AGGREGATE_ID)
			   .andEvents(accountRegistered())
			   .andMocks(this::initEmptyEmailLookup)
			   .when(activateAccount())
			   .thenExpectEvent(AccountActivated.class)
			   .matching(event -> {
				   assertThat(event.getAggregateId()).isEqualTo(AGGREGATE_ID);
			   })
			   .thenExpectNoEvents()
		;
	}

	@Test
	public void should_fail_activation_if_there_is_other_account_for_this_email_already_active() {
		fixture.given()
			   .andState(this::prepareStateForActivation)
			   .andMocks(this::initEmailLookupRandomly)
			   .when(activateAccountWithPassword())
			   .thenExpectException(AccountRegistrationException.class)
			   .withMessageContaining(EMAIL_ALREADY_TAKEN.name())
		;
	}

	@Test
	public void should_fail_activation_if_account_is_already_active() {
		fixture.given()
			   .andEvents(accountRegistered(), accountActivated())
			   .andMocks(this::initEmailLookupCorrectly)
			   .when(activateAccount())
			   .thenExpectException(AccountRegistrationException.class)
			   .withMessageContaining(EMAIL_ALREADY_TAKEN.name())
		;
	}

	@Test
	public void should_fail_activation_for_non_existing_account() {
		fixture.given()
			   .when(activateAccount())
			   .thenExpectException(AccountRegistrationException.class)
			   .withMessageContaining(ACTIVATION_TOKEN_INVALID.name())
		;
	}

	@Test
	public void should_fail_activation_before_activation_message_has_been_sent() {
		fixture.given()
			   .andState(this::initEmailAddress)
			   .andState(account -> {
				   account.activationMessageTimestamp = null;
			   })
			   .when(activateAccount())
			   .thenExpectException(AccountRegistrationException.class)
			   .withMessageContaining(ACTIVATION_TOKEN_INVALID.name())
		;
	}

	@Test
	public void should_fail_activation_after_token_expiration() {
		fixture.given()
			   .andState(this::initEmailAddress)
			   .andState(account -> {
				   account.activationMessageTimestamp = now().minus(2 * ACTIVATION_MESSAGE_EXPIRATION_PERIOD_ID_DAYS, ChronoUnit.DAYS);
			   })
			   .when(activateAccount())
			   .thenExpectException(AccountRegistrationException.class)
			   .withMessageContaining(ACTIVATION_TOKEN_EXPIRED.name())
		;
	}

	@Test
	public void should_update_state_after_activation() {
		AccountActivated event = new AccountActivated(ExpirationDuration.noExpiration(), AGGREGATE_ID, randomUUID());

		fixture.given(event)
			   .when()
			   .thenExpectNoEvents()
			   .andStateMatching(account -> {
				   assertThat(account.isActive()).isTrue();
				   assertThat(account.activationTimestamp).isEqualTo(event.getCreationTimestamp());
			   })
		;
	}

}
