package pl.oakfusion.idm;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;
import pl.oakfusion.idm.user.activation.ActivateAccount;
import pl.oakfusion.idm.user.authentication.Authenticate;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static java.util.UUID.randomUUID;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.util.Maps.newHashMap;
import static pl.oakfusion.idm.IdmCommandFactory.EMAIL_ADDRESS;
import static pl.oakfusion.idm.IdmCommandFactory.PASSWORD;
import static pl.oakfusion.idm.IdmCommandFactory.TOKEN;
import static pl.oakfusion.idm.exception.ErrorCode.ACTIVATION_TOKEN_REQUIRED;
import static pl.oakfusion.idm.exception.ErrorCode.EMAIL_AND_PASSWORD_ARE_BOTH_REQUIRED;

@RunWith(MockitoJUnitRunner.class)
public class IdmCommandFactoryTest {

	private static final String EMAIL_VALUE = "email@domain.com";
	private static final String PASSWORD_VALUE = "pass";

	@InjectMocks
	private IdmCommandFactory idm;

	// account activation

	@Test
	public void should_throw_creating_activation_command_if_token_is_missing() {
		// then
		assertThatExceptionOfType(NullPointerException.class)
				.isThrownBy(() -> idm.activateAccountByOwner(new HashMap<>()))
				.withMessageContaining(ACTIVATION_TOKEN_REQUIRED.name());
	}

	@Test
	public void should_create_activation_command_from_params() {
		// given
		UUID activationToken = randomUUID();
		Map<String, String> params = newHashMap(TOKEN, activationToken.toString());

		// when
		ActivateAccount command = idm.activateAccountByOwner(params);

		// then
		assertThat(command.getAggregateId()).isEqualTo(activationToken);
		assertThat(command.getCreatorId()).isEqualTo(command.getAggregateId());
	}

	// login 

	@Test
	public void should_throw_creating_authentication_command_if_any_required_field_missing() {
		// then
		assertThatExceptionOfType(IllegalArgumentException.class)
				.isThrownBy(() -> idm.authenticate(newHashMap(EMAIL_ADDRESS, EMAIL_VALUE)))
				.withMessageContaining(EMAIL_AND_PASSWORD_ARE_BOTH_REQUIRED.name());

		assertThatExceptionOfType(IllegalArgumentException.class)
				.isThrownBy(() -> idm.authenticate(newHashMap(PASSWORD, PASSWORD_VALUE)))
				.withMessageContaining(EMAIL_AND_PASSWORD_ARE_BOTH_REQUIRED.name());

	}

	@Test
	public void should_create_authentication_command() {
		// given
		Map<String, String> params = newHashMap(EMAIL_ADDRESS, EMAIL_VALUE);
		params.put(PASSWORD, PASSWORD_VALUE);

		// when
		Authenticate command = idm.authenticate(params);

		// then
		assertThat(command.getAggregateId()).isNull();
		assertThat(command.getCreatorId()).isNull();
		assertThat(command.getEmailAddress()).isEqualTo(EMAIL_VALUE);
		assertThat(command.getPassword()).isEqualTo(PASSWORD_VALUE);
	}

}
