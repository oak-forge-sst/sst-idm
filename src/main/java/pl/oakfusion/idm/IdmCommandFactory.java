package pl.oakfusion.idm;

import pl.oakfusion.idm.exception.ErrorCode;
import pl.oakfusion.idm.user.activation.ActivateAccount;
import pl.oakfusion.idm.user.activation.ActivateAccountWithPassword;
import pl.oakfusion.idm.user.authentication.Authenticate;

import java.util.Map;
import java.util.function.Function;

import static java.util.Objects.requireNonNull;
import static java.util.UUID.fromString;
import static pl.oakfusion.idm.exception.ErrorCode.ACTIVATION_TOKEN_REQUIRED;
import static pl.oakfusion.idm.exception.ErrorCode.EMAIL_AND_PASSWORD_ARE_BOTH_REQUIRED;

class IdmCommandFactory {

	static final String TOKEN = "token";
	static final String EMAIL_ADDRESS = "emailAddress";
	static final String PASSWORD = "password";
	static final String PASSWORD_CONFIRMATION = "passwordConfirmation";

	private Function<String, String> rise(ErrorCode error) {
		return s -> {
			throw new IllegalArgumentException(error.name());
		};
	}

	ActivateAccount activateAccountByOwner(Map<String, String> params) {
		String aggregateId = requireNonNull(params.get(TOKEN), ACTIVATION_TOKEN_REQUIRED.name());
		return new ActivateAccount(fromString(aggregateId));
	}

	ActivateAccountWithPassword activateAccountByOwnerWithPassword(Map<String, String> params) {
		String aggregateId = requireNonNull(params.get(TOKEN), ACTIVATION_TOKEN_REQUIRED.name());
		String password = params.computeIfAbsent(PASSWORD, rise(EMAIL_AND_PASSWORD_ARE_BOTH_REQUIRED));
		String passwordConfirmation = params.computeIfAbsent(PASSWORD_CONFIRMATION, rise(EMAIL_AND_PASSWORD_ARE_BOTH_REQUIRED));
		return new ActivateAccountWithPassword(fromString(aggregateId), password, passwordConfirmation);
	}

	Authenticate authenticate(Map<String, String> params) {
		String emailAddress = params.computeIfAbsent(EMAIL_ADDRESS, rise(EMAIL_AND_PASSWORD_ARE_BOTH_REQUIRED));
		String password = params.computeIfAbsent(PASSWORD, rise(EMAIL_AND_PASSWORD_ARE_BOTH_REQUIRED));
		return new Authenticate(emailAddress, password);
	}

}
