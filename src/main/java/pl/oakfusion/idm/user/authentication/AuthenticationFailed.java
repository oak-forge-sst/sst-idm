package pl.oakfusion.idm.user.authentication;

import pl.oakfusion.idm.exception.ErrorCode;
import pl.oakfusion.idm.security.AuthenticationFailure;
import pl.oakfusion.roots.api.event.Event;

import java.util.UUID;

import static pl.oakfusion.idm.security.AuthenticationException.authenticationException;

public final class AuthenticationFailed extends Event<ErrorCode> implements AuthenticationFailure {

	public AuthenticationFailed(ErrorCode cause) {
		super(cause);
	}

	public AuthenticationFailed(UUID aggregateId, UUID creatorId, ErrorCode payload) {
		super(aggregateId, creatorId, payload);
	}

	@Override
	public void fail(String message) {
		throw authenticationException(getPayload());
	}

}
