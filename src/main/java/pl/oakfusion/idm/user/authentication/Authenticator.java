package pl.oakfusion.idm.user.authentication;

import pl.oakfusion.idm.EmailLookup;
import pl.oakfusion.idm.security.AuthenticationException;
import pl.oakfusion.idm.security.PasswordEncryptor;
import pl.oakfusion.idm.user.AccountState;
import pl.oakfusion.idm.user.UserAggregate.Account;
import pl.oakfusion.idm.user.expiration.AccountExpired;
import pl.oakfusion.roots.api.event.Event;
import pl.oakfusion.idm.security.SecurityConstants;
import pl.oakfusion.security.TokenManager;

import java.util.*;

import static java.time.Instant.now;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static pl.oakfusion.idm.exception.ErrorCode.ACCOUNT_BLOCKED;
import static pl.oakfusion.idm.exception.ErrorCode.ACCOUNT_EXPIRED;
import static pl.oakfusion.idm.exception.ErrorCode.EMAIL_AND_PASSWORD_ARE_BOTH_REQUIRED;
import static pl.oakfusion.idm.exception.ErrorCode.INCORRECT_LOGIN_OR_PASSWORD;
import static pl.oakfusion.idm.security.AuthenticationException.authenticationException;
import static pl.oakfusion.idm.user.AccountState.BLOCKED;
import static pl.oakfusion.idm.user.AccountState.EXPIRED;
import static pl.oakfusion.idm.user.authentication.AuthValuesKeys.*;
import static pl.oakfusion.roots.impl.util.Utils.isEmpty;
import static pl.oakfusion.roots.impl.validator.EmailValidator.isValid;

public final class Authenticator {

	private final PasswordEncryptor passwordEncryptor;
	private final EmailLookup emailLookup;
	private final int failedAuthenticationThreshold;
	private final SecurityConstants securityConstants;
	private final TokenManager tokenManager;

	public Authenticator(PasswordEncryptor passwordEncryptor, EmailLookup emailLookup, int failedAuthenticationThreshold, SecurityConstants securityConstants, TokenManager tokenManager) {
		this.passwordEncryptor = passwordEncryptor;
		this.emailLookup = emailLookup;
		this.failedAuthenticationThreshold = failedAuthenticationThreshold;
		this.securityConstants = securityConstants;
		this.tokenManager = tokenManager;
	}

	public void prepareAuthentication(Authenticate command) {
		UUID aggregateId = getAggregateIdFromLookup(command.getEmailAddress());
		command.setAggregateId(aggregateId);
		command.setCreatorId(aggregateId);
	}

	public void validateAuthentication(Authenticate command) {
		if (!isValid(command.getEmailAddress()) || isEmpty(command.getPassword())) {
			throw authenticationException(EMAIL_AND_PASSWORD_ARE_BOTH_REQUIRED);
		}
	}

	public List<Event> authenticate(Authenticate command, Account account) {
		System.out.println(securityConstants.getKey());
		String emailAddress = account.emailAddress;
		String passwordHash = account.passwordHash;
		int authenticationFailureCount = account.authenticationFailureCount;
		AccountState accountState = account.state;

		if (accountState == EXPIRED) {
			return singletonList(accountExpiredAuthenticationFailed());
		}

		if (accountState == BLOCKED) {
			return singletonList(accountBlockedAuthenticationFailed());
		}

		if (account.isExpired(now())) {
			return asList(accountExpiredAuthenticationFailed(), new AccountExpired());
		}

		if (authenticationFailureCount >= failedAuthenticationThreshold) {
			return asList(accountBlockedAuthenticationFailed(), new AccountBlocked());
		}

		boolean emailAddressRegistered = emailAddress.equals(command.getEmailAddress());
		boolean passwordVerified = passwordEncryptor.checkPassword(command.getPassword(), passwordHash);

		if (!emailAddressRegistered || !passwordVerified || !account.isActive()) {
			return singletonList(new AuthenticationFailed(INCORRECT_LOGIN_OR_PASSWORD));
		}

		HashMap<String, String> userPayload = new HashMap<>();
		userPayload.put(TOKEN.name(), createJwtToken(account));

		return singletonList(new Authenticated(userPayload));
	}

	private String createJwtToken(Account account) {
		return tokenManager.createToken(account.emailAddress, account.roles);
	}

	public void onAuthenticated(Authenticated event, Account account) {
		account.lastSuccessfulAuthenticationTimestamp = event.getCreationTimestamp();
		account.successfulAuthenticationCount++;
		account.authenticationFailureCount = 0;
	}

	public void onAuthenticationFailed(AuthenticationFailed event, Account account) {
		account.lastAuthenticationFailureTimestamp = event.getCreationTimestamp();
		account.authenticationFailureCount++;
	}

	public void onAccountBlocked(AccountBlocked event, Account account) {
		account.state = BLOCKED;
	}

	public void onAccountExpired(AccountExpired event, Account account) {
		account.state = EXPIRED;
	}

	private UUID getAggregateIdFromLookup(String emailAddress) {
		return emailLookup.get(emailAddress).orElseThrow(Authenticator::noAggregateFound);
	}

	private static AuthenticationFailed accountBlockedAuthenticationFailed() {
		return new AuthenticationFailed(ACCOUNT_BLOCKED);
	}

	private static AuthenticationFailed accountExpiredAuthenticationFailed() {
		return new AuthenticationFailed(ACCOUNT_EXPIRED);
	}

	private static AuthenticationException noAggregateFound() {
		return authenticationException(INCORRECT_LOGIN_OR_PASSWORD);
	}

}
