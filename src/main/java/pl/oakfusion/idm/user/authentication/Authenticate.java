package pl.oakfusion.idm.user.authentication;

import pl.oakfusion.roots.api.command.Command;

import static java.lang.String.format;
import static pl.oakfusion.roots.impl.util.Utils.anonymize;

public final class Authenticate extends Command {

	private static final long serialVersionUID = 1L;

	private final String emailAddress;
	private final String password;

	public Authenticate(String emailAddress, String password) {
		super(null);
		this.emailAddress = emailAddress;
		this.password = password;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public String getPassword() {
		return password;
	}

	@Override
	protected String toDetailedString() {
		return format("emailAddress: %s, password: %s", emailAddress, anonymize(password));
	}

}
