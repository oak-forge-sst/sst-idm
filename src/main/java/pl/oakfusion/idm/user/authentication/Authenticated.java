package pl.oakfusion.idm.user.authentication;

import pl.oakfusion.roots.api.event.Event;
import pl.oakfusion.roots.api.event.trait.Values;

import java.util.HashMap;

public final class Authenticated extends Event<HashMap<String, String>> implements Values {
	protected  Authenticated() { }

	public Authenticated(HashMap<String, String> userPayload) {
		super(userPayload);
	}

	@Override
	public HashMap<String, String> values() {
		return getPayload();
	}
}
