package pl.oakfusion.idm.user.logout;

import pl.oakfusion.idm.EmailLookup;
import pl.oakfusion.idm.exception.ErrorCode;
import pl.oakfusion.idm.security.*;
import pl.oakfusion.idm.user.UserAggregate.Account;
import pl.oakfusion.roots.api.event.Event;
import pl.oakfusion.security.TokenManager;

import java.util.*;

public final class Logout {

    private final EmailLookup emailLookup;
    private final BlacklistJWTLookup blacklistJWTLookup;
    private final TokenManager tokenManager;

    private final String LOGOUT = "PERFORMED_LOGOUT";

    public Logout(EmailLookup emailLookup, BlacklistJWTLookup blacklistJWTLookup, TokenManager tokenManager) {
        this.emailLookup = emailLookup;
        this.blacklistJWTLookup = blacklistJWTLookup;
        this.tokenManager = tokenManager;
    }

    private static NoSuchEmailException noAggregateFound() {
        return NoSuchEmailException.emailException(ErrorCode.NO_SUCH_EMAIL);
    }

    public void prepareLogout(LogoutUser command) {
        String email = getEmailFromToken(command.getToken());
        UUID aggregateId = getAggregateIdFromLookup(email);
        command.setAggregateId(aggregateId);
        command.setCreatorId(aggregateId);
    }

    public List<Event> logoutUser(LogoutUser command, Account account) {
        blacklistJWTLookup.put(command.getToken(), command.getAggregateId());
        return Collections.singletonList(
                new LoggedOut(LOGOUT)
        );
    }

    public void onLoggedOut(LoggedOut event, Account account) {
        account.lastLogoutTimestamp = event.getCreationTimestamp();
    }

    private UUID getAggregateIdFromLookup(String emailAddress) {
        return emailLookup.get(emailAddress).orElseThrow(Logout::noAggregateFound);
    }

    private String getEmailFromToken(String token) {
        return tokenManager.getEmailFromToken(token);
    }
}
