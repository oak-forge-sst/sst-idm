package pl.oakfusion.idm.user.logout;

import pl.oakfusion.roots.api.command.Command;

import static java.lang.String.format;

public final class LogoutUser extends Command {

	private static final long serialVersionUID = 1L;
	private final String token;

	public LogoutUser(String token) {
		super(null);
		this.token = token;
	}

	public String getToken() {
		return token;
	}

	@Override
	public String toString() {
		return format("token: %s", getToken());
	}
}
