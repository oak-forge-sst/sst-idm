package pl.oakfusion.idm.user.logout;

import pl.oakfusion.roots.api.event.Event;

public final class LoggedOut extends Event<String> {

	protected LoggedOut() {}

	public LoggedOut(String message) {
		super(message);
	}

}
