package pl.oakfusion.idm.user.activation;

import pl.oakfusion.idm.user.expiration.ExpirationDuration;
import pl.oakfusion.roots.api.event.Event;
import pl.oakfusion.roots.api.event.trait.HiddenIdentity;

import java.util.UUID;

public final class AccountActivated extends Event implements HiddenIdentity {

	private static final long serialVersionUID = 1L;


	private final ExpirationDuration expirationDuration;

	public AccountActivated(ExpirationDuration expirationDuration) {
		this.expirationDuration = expirationDuration;
	}

	public AccountActivated(ExpirationDuration expirationDuration, UUID aggregateId, UUID creatorId) {
		super(aggregateId, creatorId);
		this.expirationDuration = expirationDuration;
	}

	public ExpirationDuration getExpirationDuration() {
		return expirationDuration;
	}
}
