package pl.oakfusion.idm.user.activation;

import pl.oakfusion.roots.api.command.Command;

import java.util.UUID;

public class ActivateAccountWithPassword extends Command {

	private static final long serialVersionUID = 1L;
	private final String password;
	private final String passwordConfirmation;

	public ActivateAccountWithPassword(UUID aggregateId, String password, String passwordConfirmation) {
		super(aggregateId, aggregateId);
		this.password = password;
		this.passwordConfirmation = passwordConfirmation;
	}

	public String getPassword() {
		return password;
	}

	public String getPasswordConfirmation() {
		return passwordConfirmation;
	}
}
