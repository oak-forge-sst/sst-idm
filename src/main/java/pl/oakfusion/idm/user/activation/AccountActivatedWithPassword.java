package pl.oakfusion.idm.user.activation;

import pl.oakfusion.idm.user.expiration.ExpirationDuration;
import pl.oakfusion.roots.api.event.Event;
import pl.oakfusion.roots.api.event.trait.HiddenIdentity;

import java.util.UUID;

public class AccountActivatedWithPassword extends Event<String> implements HiddenIdentity {
	private static final long serialVersionUID = 1L;

	private final ExpirationDuration expirationDuration;


	public AccountActivatedWithPassword(ExpirationDuration expirationDuration, UUID aggregateId, UUID creatorId, String passwordHash) {
		super(aggregateId, creatorId, passwordHash);
		this.expirationDuration = expirationDuration;
	}

	public ExpirationDuration getExpirationDuration() {
		return expirationDuration;
	}
}
