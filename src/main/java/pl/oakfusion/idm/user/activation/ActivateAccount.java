package pl.oakfusion.idm.user.activation;

import com.fasterxml.jackson.annotation.JsonCreator;
import pl.oakfusion.roots.api.command.Command;

import java.util.UUID;

public final class ActivateAccount extends Command {

	private static final long serialVersionUID = 1L;

	@JsonCreator
	public ActivateAccount(UUID aggregateId) {
		super(aggregateId, aggregateId);
	}

}
