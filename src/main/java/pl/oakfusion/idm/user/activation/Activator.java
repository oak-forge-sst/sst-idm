package pl.oakfusion.idm.user.activation;

import pl.oakfusion.idm.EmailLookup;
import pl.oakfusion.idm.exception.AccountRegistrationException;
import pl.oakfusion.idm.security.PasswordEncryptor;
import pl.oakfusion.idm.user.UserAggregate.Account;
import pl.oakfusion.idm.user.expiration.AccountExpirationStrategy;
import pl.oakfusion.idm.user.expiration.ExpirationDuration;
import pl.oakfusion.roots.api.event.Event;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static java.time.Instant.now;
import static java.util.Collections.singletonList;
import static pl.oakfusion.idm.exception.ErrorCode.*;
import static pl.oakfusion.idm.user.AccountState.ACTIVE;
import static pl.oakfusion.idm.user.password.PasswordValidator.validatePassword;
import static pl.oakfusion.roots.impl.util.Utils.isEmpty;

public final class Activator {

	private final Duration activationMessageExpiration;
	private final AccountExpirationStrategy expirationStrategy;
	private final EmailLookup emailLookup;
	private final PasswordEncryptor passwordEncryptor;

	public Activator(Duration activationMessageExpiration, AccountExpirationStrategy expirationStrategy, EmailLookup emailLookup, PasswordEncryptor passwordEncryptor) {
		this.activationMessageExpiration = activationMessageExpiration;
		this.expirationStrategy = expirationStrategy;
		this.emailLookup = emailLookup;
		this.passwordEncryptor = passwordEncryptor;
	}

	interface AccountActivatedConstructor {
		Event construct(ExpirationDuration expirationDuration, UUID aggregateId, UUID creatorId, String passwordHash);
	}

	public void validateAccountActivationWithPassword(ActivateAccountWithPassword command) {
		validatePassword(command.getPassword(), command.getPasswordConfirmation());
	}

	public List<Event> activateAccount(ActivateAccount command, Account account) {
		return activateAccount(
				command.getAggregateId(),
				command.getCreatorId(),
				account,
				Optional.empty(),
				(expirationDuration, aggregateId, creatorId, passwordHash) -> new AccountActivated(expirationDuration, aggregateId, creatorId)
		);
	}

	public List<Event> activateAccount(ActivateAccountWithPassword command, Account account) {
		String passwordHash = passwordEncryptor.encryptPassword(command.getPassword());
		return activateAccount(command.getAggregateId(), command.getCreatorId(), account, Optional.of(passwordHash), AccountActivatedWithPassword::new);
	}

	private List<Event> activateAccount(UUID aggregateId, UUID creatorId, Account account, Optional<String> passwordHash, AccountActivatedConstructor constructor) {
		String emailAddress = account.emailAddress;
		Instant activationMessageTimestamp = account.activationMessageTimestamp;
		validateActivationPreconditions(aggregateId, emailAddress, activationMessageTimestamp, passwordHash.orElse(account.passwordHash));
		checkAvailabilityOf(emailAddress);
		emailLookup.put(emailAddress, aggregateId);
		ExpirationDuration expirationDuration = expirationStrategy.getAccountValidityDuration();
		return singletonList(constructor.construct(expirationDuration, aggregateId, creatorId, passwordHash.orElse(null)));
	}

	public void onAccountActivated(AccountActivated event, Account account) {
		account.state = ACTIVE;
		account.activationTimestamp = event.getCreationTimestamp();
		account.expirationDuration = event.getExpirationDuration();
	}

	public void onAccountActivated(AccountActivatedWithPassword event, Account account) {
		account.state = ACTIVE;
		account.activationTimestamp = event.getCreationTimestamp();
		account.expirationDuration = event.getExpirationDuration();
		account.passwordHash = event.getPayload();
	}

	private void validateActivationPreconditions(UUID aggregateId, String emailAddress, Instant activationMessageTimestamp, String passwordHash) {
		if (isEmpty(emailAddress) || activationMessageTimestamp == null) {
			throw new AccountRegistrationException(ACTIVATION_TOKEN_INVALID, aggregateId.toString());
		}
		if (activationMessageTimestamp.plus(activationMessageExpiration).isBefore(now())) {
			throw new AccountRegistrationException(ACTIVATION_TOKEN_EXPIRED);
		}
		if (passwordHash == null) {
			throw new AccountRegistrationException(NO_PASSWORD_PROVIDED);
		}
	}

	private void checkAvailabilityOf(String emailAddress) {
		if (emailLookup.get(emailAddress).isPresent()) {
			throw new AccountRegistrationException(EMAIL_ALREADY_TAKEN, emailAddress);
		}
	}
}
