package pl.oakfusion.idm.user;

import pl.oakfusion.idm.EmailLookup;
import pl.oakfusion.idm.security.*;
import pl.oakfusion.idm.user.activation.*;
import pl.oakfusion.idm.user.authentication.*;
import pl.oakfusion.idm.user.expiration.AccountExpirationStrategy;
import pl.oakfusion.idm.user.expiration.AccountExpired;
import pl.oakfusion.idm.user.expiration.ExpirationDuration;
import pl.oakfusion.idm.user.logout.LoggedOut;
import pl.oakfusion.idm.user.logout.Logout;
import pl.oakfusion.idm.user.logout.LogoutUser;
import pl.oakfusion.idm.user.password.PasswordController;
import pl.oakfusion.idm.user.password.change.ChangePassword;
import pl.oakfusion.idm.user.password.change.PasswordChangeFailed;
import pl.oakfusion.idm.user.password.change.PasswordChanged;
import pl.oakfusion.idm.user.registration.*;
import pl.oakfusion.idm.user.roles.RolesController;
import pl.oakfusion.idm.user.roles.change.GrantRole;
import pl.oakfusion.idm.user.roles.change.RevokeRole;
import pl.oakfusion.idm.user.roles.change.RoleGranted;
import pl.oakfusion.idm.user.roles.change.RoleRevoked;
import pl.oakfusion.idm_commands.Roles;
import pl.oakfusion.roots.api.aggregate.AggregateState;
import pl.oakfusion.roots.api.mail.EmailManager;
import pl.oakfusion.roots.impl.aggregate.AbstractAggregateRoot;
import pl.oakfusion.security.TokenManager;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

public class UserAggregate extends AbstractAggregateRoot<UserAggregate.Account> {

	public static class Account extends AggregateState {

		private static final long serialVersionUID = 1L;

		public String emailAddress;
		public String passwordHash;

		public List<String> roles;

		public AccountState state;

		public Instant activationTimestamp;
		public Instant activationMessageTimestamp;
		public ExpirationDuration expirationDuration;

		public Instant lastSuccessfulAuthenticationTimestamp;
		public int successfulAuthenticationCount;

		public Instant lastAuthenticationFailureTimestamp;
		public int authenticationFailureCount;

		public Instant lastLogoutTimestamp;

		private Account(UUID aggregateId) {
			super(aggregateId);
			this.state = AccountState.INACTIVE;
		}

		public boolean isActive() {
			return state == AccountState.ACTIVE;
		}

		public boolean isExpired(Instant currentTime) {
			return expirationDuration.isExpired(activationTimestamp, currentTime);
		}

	}

	public UserAggregate(long activationMessageExpirationPeriodInDays, int failedAuthenticationThreshold,
						 EmailLookup emailLookup, AccountExpirationStrategy expirationStrategy,
						 EmailManager emailManager, PasswordEncryptor passwordEncryptor, SecurityConstants securityConstants,
						 Roles roles, BlacklistJWTLookup blacklistJWTLookup, TokenManager tokenManager) {

		Duration activationMessageExpiration = Duration.ofDays(activationMessageExpirationPeriodInDays);

		Register register = new Register(passwordEncryptor, emailManager, emailLookup, roles);
		Activator activator = new Activator(activationMessageExpiration, expirationStrategy, emailLookup, passwordEncryptor);
		Authenticator authenticator = new Authenticator(passwordEncryptor, emailLookup, failedAuthenticationThreshold, securityConstants, tokenManager);
		Logout logout = new Logout(emailLookup, blacklistJWTLookup, tokenManager);
		PasswordController passwordController = new PasswordController(passwordEncryptor, emailLookup);
		RolesController rolesController = new RolesController(emailLookup);

		registerCommandValidator(RegisterAccountWithRole.class, register::validateRegistration);
		registerCommandHandler(RegisterAccountWithRole.class, register::registerAccount);
		registerEventHandler(AccountRegisteredWithRoles.class, register::onAccountRegisteredWithRoles);

		registerCommandValidator(RegisterAccountWithPassword.class, register::validateRegistration);
		registerCommandHandler(RegisterAccountWithPassword.class, register::registerAccount);
		registerEventHandler(AccountRegisteredWithPassword.class, register::onAccountRegistered);

		registerCommandHandler(ActivateAccount.class, activator::activateAccount);
		registerEventHandler(AccountActivated.class, activator::onAccountActivated);

		registerCommandValidator(ActivateAccountWithPassword.class, activator::validateAccountActivationWithPassword);
		registerCommandHandler(ActivateAccountWithPassword.class, activator::activateAccount);
		registerEventHandler(AccountActivatedWithPassword.class, activator::onAccountActivated);

		registerCommandPreparer(Authenticate.class, authenticator::prepareAuthentication);
		registerCommandValidator(Authenticate.class, authenticator::validateAuthentication);
		registerCommandHandler(Authenticate.class, authenticator::authenticate);
		registerEventHandler(Authenticated.class, authenticator::onAuthenticated);
		registerEventHandler(AuthenticationFailed.class, authenticator::onAuthenticationFailed);
		registerEventHandler(AccountBlocked.class, authenticator::onAccountBlocked);
		registerEventHandler(AccountExpired.class, authenticator::onAccountExpired);

		registerCommandPreparer(LogoutUser.class, logout::prepareLogout);
		registerCommandHandler(LogoutUser.class, logout::logoutUser);
		registerEventHandler(LoggedOut.class, logout::onLoggedOut);

		registerCommandPreparer(ChangePassword.class, passwordController::preparePasswordChange);
		registerCommandValidator(ChangePassword.class, passwordController::validatePasswordChange);
		registerCommandHandler(ChangePassword.class, passwordController::changePassword);
		registerEventHandler(PasswordChanged.class, passwordController::onPasswordChange);
		registerEventHandler(PasswordChangeFailed.class, passwordController::onPasswordChangeFailed);

		registerCommandPreparer(GrantRole.class, rolesController::prepareChangeRole);
		registerCommandHandler(GrantRole.class, rolesController::grantRole);
		registerEventHandler(RoleGranted.class, rolesController::onRoleGranted);

		registerCommandPreparer(RevokeRole.class, rolesController::prepareChangeRole);
		registerCommandHandler(RevokeRole.class, rolesController::revokeRole);
		registerEventHandler(RoleRevoked.class, rolesController::onRoleRevoked);

	}

	@Override
	public Account initState(UUID aggregateId) {
		return new Account(aggregateId);
	}

}
