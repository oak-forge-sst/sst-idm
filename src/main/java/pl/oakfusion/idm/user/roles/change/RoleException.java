package pl.oakfusion.idm.user.roles.change;

import pl.oakfusion.roots.api.exception.BusinessException;

public class RoleException extends BusinessException {
	private static final long serialVersionUID = 1L;

	public RoleException(Enum errorCode) {
		super(errorCode);
	}

}
