package pl.oakfusion.idm.user.roles.change;

import pl.oakfusion.roots.api.event.Event;

import java.util.UUID;

public class RoleGranted extends Event<String> {

	public RoleGranted(UUID aggregateId, UUID creatorId, String role) {
		super(aggregateId, creatorId, role);
	}
}
