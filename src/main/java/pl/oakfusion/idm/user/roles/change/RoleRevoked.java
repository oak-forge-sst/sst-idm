package pl.oakfusion.idm.user.roles.change;

import pl.oakfusion.roots.api.event.Event;

import java.util.UUID;

public class RoleRevoked extends Event<String> {
	public RoleRevoked(UUID aggregateId, UUID creatorId, String role) {
		super(aggregateId, creatorId, role);
	}
}
