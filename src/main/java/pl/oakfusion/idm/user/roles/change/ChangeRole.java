package pl.oakfusion.idm.user.roles.change;

import pl.oakfusion.roots.api.command.Command;

public abstract class ChangeRole extends Command {
	private final String userEmailAddress;
	private final String issuer;
	private final String role;

	public ChangeRole(String userEmailAddress, String issuer, String role) {
		this.userEmailAddress = userEmailAddress;
		this.issuer = issuer;
		this.role = role;
	}

	public String getUserEmailAddress() {
		return userEmailAddress;
	}

	public String getRole() {
		return role;
	}

	public String getIssuer() {
		return issuer;
	}
}
