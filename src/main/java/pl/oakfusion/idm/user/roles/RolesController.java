package pl.oakfusion.idm.user.roles;

import pl.oakfusion.idm.EmailLookup;
import pl.oakfusion.idm.user.UserAggregate;
import pl.oakfusion.idm.user.roles.change.*;
import pl.oakfusion.roots.api.event.Event;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static pl.oakfusion.idm.exception.ErrorCode.*;

public class RolesController {
	private final EmailLookup emailLookup;

	public RolesController(EmailLookup emailLookup) {
		this.emailLookup = emailLookup;
	}

	public void prepareChangeRole(ChangeRole command) {
		UUID aggregateId = getAggregateIdFromLookup(command.getUserEmailAddress());
		UUID creatorId = getAggregateIdFromLookup(command.getIssuer());
		command.setAggregateId(aggregateId);
		command.setCreatorId(creatorId);
	}

	public List<Event> grantRole(GrantRole command, UserAggregate.Account account) {
		if(account.roles.contains(command.getRole())) {
			throw new RoleException(ROLE_ALREADY_GRANTED);
		}
		return Collections.singletonList(
				new RoleGranted(command.getAggregateId(), command.getCreatorId(), command.getRole())
		);
	}

	public List<Event> revokeRole(RevokeRole command, UserAggregate.Account account) {
		if(!account.roles.contains(command.getRole())) {
			throw new RoleException(ROLE_NOT_GRANTED);
		}

		return Collections.singletonList(
				new RoleRevoked(command.getAggregateId(), command.getCreatorId(), command.getRole())
		);
	}

	public void onRoleGranted(RoleGranted event, UserAggregate.Account account) {
		account.roles.add(event.getPayload());
	}

	public  void onRoleRevoked(RoleRevoked event, UserAggregate.Account account) {
		account.roles.remove(event.getPayload());
	}

	private UUID getAggregateIdFromLookup(String emailAddress) {
		return emailLookup.get(emailAddress).orElseThrow(() -> new RoleException(INVALID_EMAIL_ADDRESS));
	}
}
