package pl.oakfusion.idm.user.roles.change;

public class RevokeRole extends ChangeRole {
	public RevokeRole(String userEmailAddress, String issuer, String role) {
		super(userEmailAddress, issuer, role);
	}
}
