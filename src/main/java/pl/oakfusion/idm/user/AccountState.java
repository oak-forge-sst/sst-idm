package pl.oakfusion.idm.user;

public enum AccountState {
	INACTIVE, ACTIVE, BLOCKED, EXPIRED
}
