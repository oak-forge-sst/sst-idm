package pl.oakfusion.idm.user.password.change;

import pl.oakfusion.roots.api.exception.BusinessException;

public class PasswordChangeException extends BusinessException {

	private static final long serialVersionUID = 1L;

	public PasswordChangeException(Enum errorCode) {
		super(errorCode);
	}

}
