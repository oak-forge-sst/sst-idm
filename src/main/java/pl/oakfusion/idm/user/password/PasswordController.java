package pl.oakfusion.idm.user.password;

import pl.oakfusion.idm.EmailLookup;
import pl.oakfusion.idm.security.PasswordEncryptor;
import pl.oakfusion.idm.user.AccountState;
import pl.oakfusion.idm.user.UserAggregate;
import pl.oakfusion.idm.user.UserAggregate.Account;
import pl.oakfusion.idm.user.expiration.AccountExpired;
import pl.oakfusion.idm.user.password.change.ChangePassword;
import pl.oakfusion.idm.user.password.change.PasswordChangeException;
import pl.oakfusion.idm.user.password.change.PasswordChangeFailed;
import pl.oakfusion.idm.user.password.change.PasswordChanged;
import pl.oakfusion.roots.api.command.Command;
import pl.oakfusion.roots.api.event.Event;

import java.util.List;
import java.util.UUID;

import static java.time.Instant.now;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static pl.oakfusion.idm.exception.ErrorCode.ACCOUNT_BLOCKED;
import static pl.oakfusion.idm.exception.ErrorCode.ACCOUNT_EXPIRED;
import static pl.oakfusion.idm.exception.ErrorCode.INCORRECT_LOGIN_OR_PASSWORD;
import static pl.oakfusion.idm.exception.ErrorCode.PASSWORD_AND_PASSWORD_CONFIRMATION_ARE_BOTH_REQUIRED;
import static pl.oakfusion.idm.exception.ErrorCode.PASSWORD_AND_PASSWORD_CONFIRMATION_DO_NOT_MATCH;
import static pl.oakfusion.idm.user.AccountState.BLOCKED;
import static pl.oakfusion.idm.user.AccountState.EXPIRED;
import static pl.oakfusion.roots.impl.util.Utils.isEmpty;

public final class PasswordController {

	private final PasswordEncryptor passwordEncryptor;
	private final EmailLookup emailLookup;

	public PasswordController(PasswordEncryptor passwordEncryptor, EmailLookup emailLookup) {
		this.passwordEncryptor = passwordEncryptor;
		this.emailLookup = emailLookup;
	}

	public void preparePasswordChange(ChangePassword command) {
		UUID aggregateId = getAggregateIdFromLookup(command.getEmailAddress());
		attachAggregateId(command, aggregateId);
	}

	public void validatePasswordChange(ChangePassword command) {
		String newPassword = command.getNewPassword();
		String newPasswordConfirmation = command.getNewPasswordConfirmation();
		if (isEmpty(newPassword) || isEmpty(newPasswordConfirmation)) {
			throw new PasswordChangeException(PASSWORD_AND_PASSWORD_CONFIRMATION_ARE_BOTH_REQUIRED);
		}
		if (!newPassword.equals(newPasswordConfirmation)) {
			throw new PasswordChangeException(PASSWORD_AND_PASSWORD_CONFIRMATION_DO_NOT_MATCH);
		}
	}

	public List<Event> changePassword(ChangePassword command, Account account) {

		String emailAddress = account.emailAddress;
		String oldPasswordHash = account.passwordHash;
		AccountState accountState = account.state;

		if (accountState == EXPIRED) {
			return singletonList(accountExpiredPasswordChangeFailed());
		}

		if (accountState == BLOCKED) {
			return singletonList(accountBlockedPasswordChangeFailed());
		}

		if (account.isExpired(now())) {
			return asList(accountExpiredPasswordChangeFailed(), new AccountExpired());
		}

		boolean emailAddressRegistered = emailAddress.equals(command.getEmailAddress());
		boolean passwordVerified = passwordEncryptor.checkPassword(command.getPreviousPassword(), oldPasswordHash);

		if (!emailAddressRegistered || !passwordVerified || !account.isActive()) {
			return singletonList(new PasswordChangeFailed(INCORRECT_LOGIN_OR_PASSWORD));
		}

		String passwordHash = passwordEncryptor.encryptPassword(command.getNewPassword());
		PasswordChanged passwordChanged = new PasswordChanged(command.getAggregateId(), command.getCreatorId(), passwordHash);
		return singletonList(passwordChanged);

	}

	public void onPasswordChange(PasswordChanged event, UserAggregate.Account account) {
		account.passwordHash = event.getPayload();
	}

	public void onPasswordChangeFailed(PasswordChangeFailed event, Account account) {
		//empty
	}

	private UUID getAggregateIdFromLookup(String emailAddress) {
		return emailLookup.get(emailAddress).orElseThrow(() -> new PasswordChangeException(INCORRECT_LOGIN_OR_PASSWORD));
	}

	private void attachAggregateId(Command command, UUID aggregateId) {
		command.setAggregateId(aggregateId);
		command.setCreatorId(aggregateId);
	}

	private static PasswordChangeFailed accountBlockedPasswordChangeFailed() {
		return new PasswordChangeFailed(ACCOUNT_BLOCKED);
	}

	private static PasswordChangeFailed accountExpiredPasswordChangeFailed() {
		return new PasswordChangeFailed(ACCOUNT_EXPIRED);
	}

}
