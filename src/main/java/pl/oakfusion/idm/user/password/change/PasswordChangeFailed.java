package pl.oakfusion.idm.user.password.change;

import pl.oakfusion.idm.exception.ErrorCode;
import pl.oakfusion.roots.api.event.Event;
import pl.oakfusion.roots.api.event.trait.Failure;

import java.util.UUID;

public final class PasswordChangeFailed extends Event<ErrorCode> implements Failure {

	private static final long serialVersionUID = 1L;

	public PasswordChangeFailed(ErrorCode cause) {
		super(cause);
	}

	public PasswordChangeFailed(UUID aggregateId, UUID creatorId, ErrorCode payload) {
		super(aggregateId, creatorId, payload);
	}

	@Override
	public void fail(String s) {
		throw new PasswordChangeException(getPayload());
	}

}
