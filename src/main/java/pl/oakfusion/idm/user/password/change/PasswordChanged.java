package pl.oakfusion.idm.user.password.change;

import pl.oakfusion.roots.api.event.Event;
import pl.oakfusion.roots.api.event.trait.HiddenIdentity;

import java.util.UUID;

public final class PasswordChanged extends Event<String> implements HiddenIdentity {

	public PasswordChanged(UUID aggregateId, UUID creatorId, String newPasswordHash) {
		super(aggregateId, creatorId, newPasswordHash);
	}

}
