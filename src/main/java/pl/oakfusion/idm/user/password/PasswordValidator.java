package pl.oakfusion.idm.user.password;

import pl.oakfusion.idm.exception.AccountRegistrationException;

import static pl.oakfusion.idm.exception.ErrorCode.PASSWORD_AND_PASSWORD_CONFIRMATION_ARE_BOTH_REQUIRED;
import static pl.oakfusion.idm.exception.ErrorCode.PASSWORD_AND_PASSWORD_CONFIRMATION_DO_NOT_MATCH;
import static pl.oakfusion.roots.impl.util.Utils.isEmpty;

public class PasswordValidator {
	public static void validatePassword(String password, String passwordConfirmation) {
		if (isEmpty(password) || isEmpty(passwordConfirmation)) {
			throw new AccountRegistrationException(PASSWORD_AND_PASSWORD_CONFIRMATION_ARE_BOTH_REQUIRED);
		}
		if (!password.equals(passwordConfirmation)) {
			throw new AccountRegistrationException(PASSWORD_AND_PASSWORD_CONFIRMATION_DO_NOT_MATCH);
		}
	}
}
