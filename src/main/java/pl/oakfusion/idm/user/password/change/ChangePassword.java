package pl.oakfusion.idm.user.password.change;

import pl.oakfusion.roots.api.command.Command;

import static java.lang.String.format;
import static pl.oakfusion.roots.impl.util.Utils.anonymize;

public final class ChangePassword extends Command {

	private static final long serialVersionUID = 1L;

	private final String emailAddress;
	private final String previousPassword;
	private final String newPassword;
	private final String newPasswordConfirmation;

	public ChangePassword(String emailAddress, String previousPassword, String newPassword, String newPasswordConfirmation) {
		this.emailAddress = emailAddress;
		this.previousPassword = previousPassword;
		this.newPassword = newPassword;
		this.newPasswordConfirmation = newPasswordConfirmation;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public String getPreviousPassword() {
		return previousPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public String getNewPasswordConfirmation() {
		return newPasswordConfirmation;
	}

	@Override
	protected String toDetailedString() {
		return format(
				"emailAddress: %s, previousPassword: %s, newPassword: %s, newPasswordConfirmation: %s",
				emailAddress, anonymize(previousPassword), anonymize(newPassword), anonymize(newPasswordConfirmation)
		);
	}
}
