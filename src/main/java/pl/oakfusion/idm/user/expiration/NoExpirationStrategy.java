package pl.oakfusion.idm.user.expiration;

public final class NoExpirationStrategy implements AccountExpirationStrategy {

	@Override
	public ExpirationDuration getAccountValidityDuration() {
		return ExpirationDuration.noExpiration();
	}

}
