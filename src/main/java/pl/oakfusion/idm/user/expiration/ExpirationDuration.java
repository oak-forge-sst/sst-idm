package pl.oakfusion.idm.user.expiration;

import java.time.Duration;
import java.time.Instant;

public final class ExpirationDuration {

	private final Duration duration;
	private final boolean isInfinite;

	private ExpirationDuration(Duration duration, boolean isInfinite) {
		this.duration = duration;
		this.isInfinite = isInfinite;
	}

	public static ExpirationDuration ofDuration(Duration timestamp) {
		return new ExpirationDuration(timestamp, false);
	}

	public static ExpirationDuration noExpiration() {
		return new ExpirationDuration(null, true);
	}

	public boolean isExpired(Instant activationTimestamp, Instant currentTime) {
		if (isInfinite) {
			return false;
		} else {
			return activationTimestamp.plus(duration).isBefore(currentTime);
		}
	}

}
