package pl.oakfusion.idm.user.expiration;

@FunctionalInterface
public interface AccountExpirationStrategy {
	ExpirationDuration getAccountValidityDuration();
}
