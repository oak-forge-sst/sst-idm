package pl.oakfusion.idm.user.expiration;

import java.time.Duration;

public final class DurationBasedExpirationStrategy implements AccountExpirationStrategy {

	private final Duration expirationDuration;

	public DurationBasedExpirationStrategy(Duration expirationDuration) {
		this.expirationDuration = expirationDuration;
	}

	@Override
	public ExpirationDuration getAccountValidityDuration() {
		return ExpirationDuration.ofDuration(expirationDuration);
	}
}
