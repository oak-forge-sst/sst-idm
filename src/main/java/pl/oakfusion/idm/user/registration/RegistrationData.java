package pl.oakfusion.idm.user.registration;

import pl.oakfusion.roots.api.mail.Email;

import java.io.Serializable;
import java.util.ArrayList;

public abstract class RegistrationData implements Serializable {

	private final String emailAddress;
	private final Email activationEmailMessage;
	private final ArrayList<String> roles;

	public RegistrationData(String emailAddress, Email activationEmailMessage, ArrayList<String> roles) {
		this.emailAddress = emailAddress;
		this.activationEmailMessage = activationEmailMessage;
		this.roles = roles;
	}

	public ArrayList<String> getRoles() {
		return roles;
	}

	public String getEmailAddress() {
		return emailAddress;
	}


	public Email getActivationEmailMessage() {
		return activationEmailMessage;
	}
}
