package pl.oakfusion.idm.user.registration;

import pl.oakfusion.roots.api.command.Command;

import static java.lang.String.format;
import static pl.oakfusion.roots.impl.util.Utils.anonymize;

public final class RegisterAccountWithPassword extends Command {

	private static final long serialVersionUID = 1L;

	private final String emailAddress;
	private final String password;
	private final String passwordConfirmation;

	public RegisterAccountWithPassword(String emailAddress, String password, String passwordConfirmation) {
		this.emailAddress = emailAddress;
		this.password = password;
		this.passwordConfirmation = passwordConfirmation;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public String getPassword() {
		return password;
	}

	public String getPasswordConfirmation() {
		return passwordConfirmation;
	}

	@Override
	protected String toDetailedString() {
		return format("emailAddress: %s, password: %s, passwordConfirmation: %s", emailAddress, anonymize(password), anonymize(passwordConfirmation));
	}
}
