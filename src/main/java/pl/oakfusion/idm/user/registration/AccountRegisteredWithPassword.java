package pl.oakfusion.idm.user.registration;

import pl.oakfusion.roots.api.mail.Email;

import java.util.ArrayList;
import java.util.UUID;

public final class AccountRegisteredWithPassword extends AccountRegistered<AccountRegisteredWithPassword.Data> {

	private static final long serialVersionUID = 1L;

	public static class Data extends RegistrationData {

		private static final long serialVersionUID = AccountRegisteredWithPassword.serialVersionUID;

		private final String passwordHash;

		Data(String emailAddress, String passwordHash, Email activationEmailMessage, ArrayList<String> roles) {
			super(emailAddress, activationEmailMessage, roles);
			this.passwordHash = passwordHash;
		}

		public String getPasswordHash() {
			return passwordHash;
		}

	}

	public AccountRegisteredWithPassword(UUID aggregateId, UUID creatorId, String emailAddress, String passwordHash, Email activationEmailMessage, ArrayList<String> roles) {
		super(aggregateId, creatorId, new Data(emailAddress, passwordHash, activationEmailMessage, roles));
	}

}
