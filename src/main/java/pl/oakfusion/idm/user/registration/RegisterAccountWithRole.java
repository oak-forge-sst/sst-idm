package pl.oakfusion.idm.user.registration;

import pl.oakfusion.roots.api.command.Command;

import java.util.ArrayList;

import static java.lang.String.format;

public class RegisterAccountWithRole extends Command {
	private static final long serialVersionUID = 1L;

	private final String emailAddress;
	private final ArrayList<String> roles;

	public RegisterAccountWithRole(String emailAddress, ArrayList<String> roles) {
		this.emailAddress = emailAddress;
		this.roles = roles;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public ArrayList<String> getRoles() {
		return roles;
	}

	@Override
	protected String toDetailedString() {
		return format("emailAddress: %s, roles: %s", emailAddress, roles.toString());
	}

}
