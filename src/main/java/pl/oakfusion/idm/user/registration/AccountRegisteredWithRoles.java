package pl.oakfusion.idm.user.registration;

import pl.oakfusion.roots.api.mail.Email;

import java.util.ArrayList;
import java.util.UUID;

public class AccountRegisteredWithRoles extends AccountRegistered<AccountRegisteredWithRoles.Data> {
	private static final long serialVersionUID = 1L;

	public static class Data extends RegistrationData {

		private static final long serialVersionUID = AccountRegisteredWithRoles.serialVersionUID;

		Data(String emailAddress, ArrayList<String> roles, Email activationEmailMessage) {
			super(emailAddress, activationEmailMessage, roles);
		}

	}

	public AccountRegisteredWithRoles(UUID aggregateId, UUID creatorId, String emailAddress, ArrayList<String> roles, Email activationEmailMessage) {
		super(aggregateId, creatorId, new Data(emailAddress, roles, activationEmailMessage));
	}
}
