package pl.oakfusion.idm.user.registration;

import pl.oakfusion.idm.EmailLookup;
import pl.oakfusion.idm.exception.AccountRegistrationException;
import pl.oakfusion.idm.security.PasswordEncryptor;
import pl.oakfusion.idm.user.UserAggregate.Account;
import pl.oakfusion.idm_commands.Roles;
import pl.oakfusion.roots.api.event.Event;
import pl.oakfusion.roots.api.mail.Email;
import pl.oakfusion.roots.api.mail.EmailManager;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.util.Collections.singletonList;
import static java.util.UUID.randomUUID;
import static pl.oakfusion.idm.exception.ErrorCode.*;
import static pl.oakfusion.idm.user.password.PasswordValidator.validatePassword;
import static pl.oakfusion.roots.impl.validator.EmailValidator.isValid;

public final class Register {

	public static final String ACTIVATION_MESSAGE_NAME = "activation-message";
	public static final String ACTIVATION_TOKEN = "activationToken";
	public static final String USER = "user";

	private final PasswordEncryptor passwordEncryptor;
	private final EmailManager emailManager;
	private final EmailLookup emailLookup;
	private final Roles roles;

	public Register(PasswordEncryptor passwordEncryptor, EmailManager emailManager, EmailLookup emailLookup, Roles roles) {
		this.passwordEncryptor = passwordEncryptor;
		this.emailManager = emailManager;
		this.emailLookup = emailLookup;
		this.roles = roles;
	}

	public void validateRegistration(RegisterAccountWithPassword command) {
		String emailAddress = command.getEmailAddress();
		if (!isValid(emailAddress)) {
			throw new AccountRegistrationException(INVALID_EMAIL_ADDRESS_FORMAT, emailAddress);
		}
		validatePassword(command.getPassword(), command.getPasswordConfirmation());
	}

	public void validateRegistration(RegisterAccountWithRole command) {
		String emailAddress = command.getEmailAddress();
		List<String> roles = command.getRoles();
		if (!isValid(emailAddress)) {
			throw new AccountRegistrationException(INVALID_EMAIL_ADDRESS_FORMAT, emailAddress);
		}
		if(roles.isEmpty()) {
			throw new AccountRegistrationException(NO_ROLE_PROVIDED);
		}
	}

	public List<Event> registerAccount(RegisterAccountWithPassword command, Account account) {
		String emailAddress = command.getEmailAddress();
		checkAvailabilityOf(emailAddress);
		UUID aggregateId = randomUUID();
		Email activationMessage = prepareActivationMessage(emailAddress, aggregateId);
		UUID creatorId = command.getCreatorId() != null ? command.getCreatorId() : aggregateId;

		String passwordHash = passwordEncryptor.encryptPassword(command.getPassword());
		ArrayList<String> rolesList = new ArrayList<>(singletonList(roles.getDefaultRole()));
		AccountRegisteredWithPassword event = new AccountRegisteredWithPassword(aggregateId, creatorId, emailAddress, passwordHash, activationMessage, rolesList);
		return singletonList(event);
	}

	public List<Event> registerAccount(RegisterAccountWithRole command, Account account) {
		String emailAddress = command.getEmailAddress();
		checkAvailabilityOf(emailAddress);
		UUID aggregateId = randomUUID();
		Email activationMessage = prepareActivationMessage(emailAddress, aggregateId);
		UUID creatorId = command.getCreatorId() != null ? command.getCreatorId() : aggregateId;

		AccountRegisteredWithRoles event = new AccountRegisteredWithRoles(aggregateId, creatorId, emailAddress, command.getRoles(), activationMessage);
		return singletonList(event);
	}

	public void onAccountRegistered(AccountRegisteredWithPassword event, Account account) {
		AccountRegisteredWithPassword.Data data = event.getPayload();
		account.passwordHash = data.getPasswordHash();
		registerAccount(event, account);
	}

	public void onAccountRegisteredWithRoles(AccountRegisteredWithRoles event, Account account) {
		registerAccount(event, account);
	}

	private void registerAccount(AccountRegistered<?> event, Account account) {
		RegistrationData data = event.getPayload();
		account.emailAddress = data.getEmailAddress();
		if(account.roles == null) {
			account.roles = data.getRoles();
		} else {
			account.roles.addAll(data.getRoles());
		}
		account.activationMessageTimestamp = event.getCreationTimestamp();
	}

	private void checkAvailabilityOf(String emailAddress) {
		if (emailLookup.get(emailAddress).isPresent()) {
			throw new AccountRegistrationException(EMAIL_ALREADY_TAKEN, emailAddress);
		}
	}

	private Email prepareActivationMessage(String emailAddress, UUID aggregateId) {
		try {
			return emailManager.prepareAndSendMessage(b -> b
					.withTemplateName(ACTIVATION_MESSAGE_NAME)
					.withTo(emailAddress)
					.withParam(USER, emailAddress)
					.withParam(ACTIVATION_TOKEN, aggregateId)
			);
		} catch (Exception e) {
			throw new AccountRegistrationException(ACTIVATION_MESSAGE_FAILURE, e, emailAddress);
		}
	}
}
