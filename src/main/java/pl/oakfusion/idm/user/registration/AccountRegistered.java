package pl.oakfusion.idm.user.registration;

import pl.oakfusion.roots.api.event.Event;
import pl.oakfusion.roots.api.event.trait.HiddenIdentity;

import java.util.UUID;

public abstract class AccountRegistered<T extends RegistrationData> extends Event<T> implements HiddenIdentity {
	AccountRegistered(UUID aggregateId, UUID creatorId, T data) {
		super(aggregateId, creatorId, data);
	}
}
