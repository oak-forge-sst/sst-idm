package pl.oakfusion.idm.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.WebUtils;
import pl.oakfusion.idm.user.activation.ActivateAccountWithPassword;
import pl.oakfusion.idm.user.authentication.Authenticate;
import pl.oakfusion.idm.user.logout.LogoutUser;
import pl.oakfusion.idm.user.registration.RegisterAccountWithPassword;
import pl.oakfusion.roots.api.command.*;
import pl.oakfusion.security.SecurityCheck;
import javax.servlet.http.*;
import java.util.Objects;
import static pl.oakfusion.idm.Config.*;
import static pl.oakfusion.idm.user.authentication.AuthValuesKeys.TOKEN;

@RestController
public class AuthController {

	private static final String TOKEN_HEADER = "idmToken";
	private final CommandProcessor commandProcessor;
	private final SecurityCheck securityCheck;

	@Autowired
	public AuthController(CommandProcessor commandProcessor, SecurityCheck securityCheck) {
		this.commandProcessor = commandProcessor;
		this.securityCheck = securityCheck;
	}

	@PostMapping(path = "/login")
	public CommandResult performLogin(HttpServletResponse response, @RequestBody Authenticate payload) {
		payload.withName(AUTHENTICATE);
		return getCommandResult(response, payload);
	}

	@PostMapping(path = "/register")
	public CommandResult registerAccount(HttpServletResponse response, @RequestBody RegisterAccountWithPassword payload) {
		payload.withName(REGISTER_ACCOUNT);
		return getCommandResult(response, payload);
	}

	@PostMapping(path = "/activate")
	public CommandResult activateAccount(HttpServletResponse response, @RequestBody ActivateAccountWithPassword payload) {
		payload.withName(ACTIVATE_ACCOUNT_WITH_PASSWORD);
		return getCommandResult(response, payload);
	}

	@PostMapping("/logout")
	public CommandResult performLogout(HttpServletRequest request, HttpServletResponse response) {
		Cookie idmCookie = Objects.requireNonNull(WebUtils.getCookie(request, TOKEN_HEADER));
		LogoutUser logout = new LogoutUser(idmCookie.getValue());
		logout.withName(LOGOUT);
		CommandResult commandResult = CommandResult.resultOf(commandProcessor.process(logout));
		idmCookie.setHttpOnly(true);
		idmCookie.setPath("/");
		idmCookie.setMaxAge(0);
		response.addCookie(idmCookie);
		return commandResult;
	}

	@GetMapping("/check-authentication")
	public ResponseEntity<Boolean> isAuthenticated(@RequestHeader(TOKEN_HEADER) String token) {
		return ResponseEntity.ok(securityCheck.isAuthenticated(token));
	}

	private CommandResult getCommandResult(final HttpServletResponse response, @RequestBody final Command payload) {
		CommandResult commandResult = CommandResult.resultOf(commandProcessor.process(payload));
		Cookie cookie = new Cookie(TOKEN_HEADER, commandResult.getValues().get(TOKEN.name()));
		cookie.setHttpOnly(true);
		cookie.setPath("/");
		response.addCookie(cookie);
		return commandResult;
	}
}
