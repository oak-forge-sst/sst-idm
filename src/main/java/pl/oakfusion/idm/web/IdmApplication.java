package pl.oakfusion.idm.web;

import com.netflix.discovery.DiscoveryClient;
import com.netflix.discovery.shared.transport.jersey.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.cloud.client.discovery.*;
import org.springframework.context.annotation.*;
import org.springframework.core.io.*;
import pl.oakfusion.idm.*;

import java.io.*;

import static pl.oakfusion.idm.security.SslConfig.*;

@EnableDiscoveryClient
@SpringBootApplication(scanBasePackageClasses = {
		IdmApplication.class,
		Config.class
})
public class IdmApplication {

	@Value("${server.ssl.key-store-password}")
	String password;

	public static void main(String[] args) {
		SpringApplication.run(IdmApplication.class, args);
	}

	@Bean
	public DiscoveryClient.DiscoveryClientOptionalArgs discoveryClientOptionalArgs() throws IOException {
		DiscoveryClient.DiscoveryClientOptionalArgs args = new DiscoveryClient.DiscoveryClientOptionalArgs();
		String keyStorePath = new ClassPathResource(KEY_STORE_FILE_PATH).getURL().getPath();
		System.setProperty("javax.net.ssl.keyStore", keyStorePath);
		System.setProperty("javax.net.ssl.keyStorePassword", password);
		System.setProperty("javax.net.ssl.trustStore", keyStorePath);
		System.setProperty("javax.net.ssl.trustStorePassword", password);
		EurekaJerseyClientImpl.EurekaJerseyClientBuilder builder = new EurekaJerseyClientImpl.EurekaJerseyClientBuilder();
		builder.withClientName("sst-idm");
		builder.withSystemSSLConfiguration();
		builder.withMaxTotalConnections(10);
		builder.withMaxConnectionsPerHost(10);
		args.setEurekaJerseyClient(builder.build());
		return args;
	}
}
