package pl.oakfusion.idm.web;

import pl.oakfusion.idm.user.registration.RegisterAccountWithRole;
import pl.oakfusion.idm_commands.Roles;
import pl.oakfusion.roots.api.command.CommandProcessor;
import pl.oakfusion.roots.api.query.Query;

import java.util.ArrayList;
import java.util.Collections;

import static pl.oakfusion.idm.Config.*;
import static pl.oakfusion.roots.api.query.filter.Filters.eq;

public class AdminInit {
	private final CommandProcessor commandProcessor;
	private final Query query;
	private final Roles roles;

	public AdminInit(CommandProcessor commandProcessor, Query query, Roles roles) {
		this.commandProcessor = commandProcessor;
		this.query = query;
		this.roles = roles;
	}

	public void initAdmin() {
		if(query.fetch(USER_ACCOUNT_PROJECTION, () -> eq("data.roles", roles.getAdminRole())).isEmpty()) {
			commandProcessor.process(
					new RegisterAccountWithRole(
							roles.getAdminEmail(),
							new ArrayList<>(Collections.singletonList(roles.getAdminRole()))
					).withName(REGISTER_ACCOUNT_WITH_ROLE)
			);
		}
	}

}
