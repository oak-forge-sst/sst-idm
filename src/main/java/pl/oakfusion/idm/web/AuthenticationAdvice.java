package pl.oakfusion.idm.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import pl.oakfusion.idm.security.AuthenticationException;
import pl.oakfusion.roots.web.response.ErrorResponse;

@RestController()
@ControllerAdvice
public class AuthenticationAdvice {

	private static final Logger LOG = LoggerFactory.getLogger(AuthenticationAdvice.class);

	@ExceptionHandler(AuthenticationException.class)
	@ResponseStatus(HttpStatus.UNAUTHORIZED)
	public ErrorResponse handle(AuthenticationException e) {
		LOG.error("authentication failed for: " + e.getMessage());
		return ErrorResponse.of(e.getMessage());
	}


}
