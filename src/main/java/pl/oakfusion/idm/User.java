package pl.oakfusion.idm;

import pl.oakfusion.idm.user.AccountState;
import pl.oakfusion.roots.api.readmodel.ReadModel;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Objects;


class User extends ReadModel {

	private String name;
	private ArrayList<String> roles;
	private AccountState state;
	private Instant registrationTimestamp;
	private Instant activationTimestamp;
	private Instant lastSuccessfulLoginTimestamp;
	private Instant lastFailedLoginTimestamp;
	private Instant lastAccountBlockTimestamp;
	private Instant lastPasswordChangeTimestamp;
	private Instant lastLogoutTimestamp;

	private Instant lastFailedPasswordChangeTimestamp;

	public User() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public AccountState getState() {
		return state;
	}

	public void setState(AccountState state) {
		this.state = state;
	}

	public Instant getRegistrationTimestamp() {
		return registrationTimestamp;
	}

	public void setRegistrationTimestamp(Instant registrationTimestamp) {
		this.registrationTimestamp = registrationTimestamp;
	}

	public Instant getLastSuccessfulLoginTimestamp() {
		return lastSuccessfulLoginTimestamp;
	}

	public void setLastSuccessfulLoginTimestamp(Instant lastSuccessfulLoginTimestamp) {
		this.lastSuccessfulLoginTimestamp = lastSuccessfulLoginTimestamp;
	}

	public Instant getLastFailedLoginTimestamp() {
		return lastFailedLoginTimestamp;
	}

	public void setLastFailedLoginTimestamp(Instant lastFailedLoginTimestamp) {
		this.lastFailedLoginTimestamp = lastFailedLoginTimestamp;
	}

	public Instant getActivationTimestamp() {
		return activationTimestamp;
	}

	public void setActivationTimestamp(Instant activationTimestamp) {
		this.activationTimestamp = activationTimestamp;
	}

	public Instant getLastPasswordChangeTimestamp() {
		return lastPasswordChangeTimestamp;
	}

	public void setLastPasswordChangeTimestamp(Instant lastPasswordChangeTimestamp) {
		this.lastPasswordChangeTimestamp = lastPasswordChangeTimestamp;
	}

	public Instant getLastAccountBlockTimestamp() {
		return lastAccountBlockTimestamp;
	}

	public void setLastAccountBlockTimestamp(Instant lastAccountBlockTimestamp) {
		this.lastAccountBlockTimestamp = lastAccountBlockTimestamp;
	}

	public Instant getLastFailedPasswordChangeTimestamp() {
		return lastFailedPasswordChangeTimestamp;
	}

	public void setLastFailedPasswordChangeTimestamp(Instant lastFailedPasswordChangeTimestamp) {
		this.lastFailedPasswordChangeTimestamp = lastFailedPasswordChangeTimestamp;
	}

	public Instant getLastLogoutTimestamp() {
		return lastLogoutTimestamp;
	}

	public void setLastLogoutTimestamp(Instant lastLogoutTimestamp) {
		this.lastLogoutTimestamp = lastLogoutTimestamp;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		User user = (User) o;
		return Objects.equals(name, user.name) &&
				state == user.state &&
				Objects.equals(registrationTimestamp, user.registrationTimestamp) &&
				Objects.equals(activationTimestamp, user.activationTimestamp) &&
				Objects.equals(lastSuccessfulLoginTimestamp, user.lastSuccessfulLoginTimestamp) &&
				Objects.equals(lastFailedLoginTimestamp, user.lastFailedLoginTimestamp) &&
				Objects.equals(lastAccountBlockTimestamp, user.lastAccountBlockTimestamp) &&
				Objects.equals(lastPasswordChangeTimestamp, user.lastPasswordChangeTimestamp) &&
				Objects.equals(lastFailedPasswordChangeTimestamp, user.lastFailedPasswordChangeTimestamp) &&
				Objects. equals(lastLogoutTimestamp, user.lastLogoutTimestamp);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, state, registrationTimestamp, activationTimestamp, lastSuccessfulLoginTimestamp, lastFailedLoginTimestamp, lastAccountBlockTimestamp, lastPasswordChangeTimestamp, lastFailedPasswordChangeTimestamp, lastLogoutTimestamp);
	}

	public ArrayList<String> getRoles() {
		return roles;
	}

	public void setRoles(ArrayList<String> roles) {
		this.roles = roles;
	}
}
