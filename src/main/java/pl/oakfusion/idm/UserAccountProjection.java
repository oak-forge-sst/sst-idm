package pl.oakfusion.idm;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.oakfusion.idm.user.activation.AccountActivated;
import pl.oakfusion.idm.user.activation.AccountActivatedWithPassword;
import pl.oakfusion.idm.user.authentication.AccountBlocked;
import pl.oakfusion.idm.user.authentication.Authenticated;
import pl.oakfusion.idm.user.authentication.AuthenticationFailed;
import pl.oakfusion.idm.user.expiration.AccountExpired;
import pl.oakfusion.idm.user.logout.LoggedOut;
import pl.oakfusion.idm.user.password.change.PasswordChangeFailed;
import pl.oakfusion.idm.user.password.change.PasswordChanged;
import pl.oakfusion.idm.user.registration.AccountRegistered;
import pl.oakfusion.idm.user.registration.AccountRegisteredWithPassword;
import pl.oakfusion.idm.user.registration.AccountRegisteredWithRoles;
import pl.oakfusion.idm.user.registration.RegistrationData;
import pl.oakfusion.idm.user.roles.change.RoleGranted;
import pl.oakfusion.idm.user.roles.change.RoleRevoked;
import pl.oakfusion.roots.api.messagebus.MessageBus;
import pl.oakfusion.roots.api.readmodel.ReadModelStorage;
import pl.oakfusion.roots.impl.projection.DefaultProjection;

import java.time.Instant;
import java.util.ArrayList;
import java.util.function.Supplier;

import static pl.oakfusion.idm.user.AccountState.*;

class UserAccountProjection extends DefaultProjection<User> {

	private static final Logger LOG = LoggerFactory.getLogger(UserAccountProjection.class);

	UserAccountProjection(String readModelName, Supplier<User> newInstanceSupplier, ReadModelStorage readModelStorage, MessageBus messageBus) {
		super(readModelName, newInstanceSupplier, readModelStorage, messageBus);

		registerHandler(AccountRegisteredWithPassword.class, this::handle);
		registerHandler(AccountRegisteredWithRoles.class, this::handle);
		registerHandler(AccountActivated.class, this::handle);
		registerHandler(AccountActivatedWithPassword.class, this::handle);
		registerHandler(Authenticated.class, this::handle);
		registerHandler(AuthenticationFailed.class, this::handle);
		registerHandler(LoggedOut.class, this::handle);
		registerHandler(AccountBlocked.class, this::handle);
		registerHandler(PasswordChanged.class, this::handle);
		registerHandler(PasswordChangeFailed.class, this::handle);
		registerHandler(AccountExpired.class, this::handle);
		registerHandler(RoleGranted.class, this::handle);
		registerHandler(RoleRevoked.class, this::handle);

	}

	private User handle(AccountRegistered<?> event, User user) {
		RegistrationData data = event.getPayload();
		user.setState(INACTIVE);
		user.setName(data.getEmailAddress());
		user.setRegistrationTimestamp(event.getCreationTimestamp());
		user.setRoles(data.getRoles());
		return user;
	}


	private User handle(AccountActivated event, User user) {
		return activateUser(user, event.getCreationTimestamp());
	}

	private User handle(AccountActivatedWithPassword event, User user) {
		return activateUser(user, event.getCreationTimestamp());
	}

	private User activateUser(User user, Instant creationTimestamp) {
		user.setState(ACTIVE);
		user.setActivationTimestamp(creationTimestamp);
		return user;
	}

	private User handle(Authenticated event, User user) {
		user.setLastSuccessfulLoginTimestamp(event.getCreationTimestamp());
		return user;
	}

	private User handle(AuthenticationFailed event, User user) {
		user.setLastFailedLoginTimestamp(event.getCreationTimestamp());
		return user;
	}

	private User handle(LoggedOut event, User user) {
		user.setLastLogoutTimestamp(event.getCreationTimestamp());
		return user;
	}

	private User handle(AccountBlocked event, User user) {
		user.setState(BLOCKED);
		user.setLastAccountBlockTimestamp(event.getCreationTimestamp());
		return user;
	}

	private User handle(PasswordChanged event, User user) {
		user.setLastPasswordChangeTimestamp(event.getCreationTimestamp());
		return user;
	}

	private User handle(PasswordChangeFailed event, User user) {
		user.setLastFailedPasswordChangeTimestamp(event.getCreationTimestamp());
		return user;
	}

	private User handle(AccountExpired event, User user) {
		user.setState(EXPIRED);
		return user;
	}

	private User handle(RoleGranted event, User user) {
		ArrayList<String> roles = user.getRoles();
		roles.add(event.getPayload());
		user.setRoles(roles);
		return user;
	}

	private User handle(RoleRevoked event, User user) {
		ArrayList<String> roles = user.getRoles();
		roles.remove(event.getPayload());
		user.setRoles(roles);
		return user;
	}

}
