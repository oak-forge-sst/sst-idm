package pl.oakfusion.idm;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;
import com.mongodb.client.MongoDatabase;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import pl.oakfusion.idm.security.*;
import pl.oakfusion.idm.user.UserAggregate;
import pl.oakfusion.idm.user.activation.AccountActivated;
import pl.oakfusion.idm.user.activation.AccountActivatedWithPassword;
import pl.oakfusion.idm.user.activation.ActivateAccount;
import pl.oakfusion.idm.user.activation.ActivateAccountWithPassword;
import pl.oakfusion.idm.user.authentication.AccountBlocked;
import pl.oakfusion.idm.user.authentication.Authenticate;
import pl.oakfusion.idm.user.authentication.Authenticated;
import pl.oakfusion.idm.user.authentication.AuthenticationFailed;
import pl.oakfusion.idm.user.expiration.AccountExpirationStrategy;
import pl.oakfusion.idm.user.expiration.AccountExpired;
import pl.oakfusion.idm.user.expiration.NoExpirationStrategy;
import pl.oakfusion.idm.user.logout.LoggedOut;
import pl.oakfusion.idm.user.logout.LogoutUser;
import pl.oakfusion.idm.user.password.change.ChangePassword;
import pl.oakfusion.idm.user.password.change.PasswordChangeFailed;
import pl.oakfusion.idm.user.password.change.PasswordChanged;
import pl.oakfusion.idm.user.registration.AccountRegisteredWithPassword;
import pl.oakfusion.idm.user.registration.AccountRegisteredWithRoles;
import pl.oakfusion.idm.user.registration.RegisterAccountWithPassword;
import pl.oakfusion.idm.user.registration.RegisterAccountWithRole;
import pl.oakfusion.idm.user.roles.change.GrantRole;
import pl.oakfusion.idm.user.roles.change.RevokeRole;
import pl.oakfusion.idm.user.roles.change.RoleGranted;
import pl.oakfusion.idm.user.roles.change.RoleRevoked;
import pl.oakfusion.idm.web.AdminInit;
import pl.oakfusion.idm_commands.Roles;
import pl.oakfusion.roots.api.command.CommandProcessor;
import pl.oakfusion.roots.api.context.Context;
import pl.oakfusion.roots.api.eventstore.storage.EventStorage;
import pl.oakfusion.roots.api.mail.EmailManager;
import pl.oakfusion.roots.api.messagebus.MessageBus;
import pl.oakfusion.roots.api.query.Query;
import pl.oakfusion.roots.api.readmodel.ReadModelStorage;
import pl.oakfusion.roots.api.type.TypeRegistry;
import pl.oakfusion.roots.external.mongo.eventstore.storage.MongoEventStorage;
import pl.oakfusion.roots.external.mongo.query.MongoFilterMapper;
import pl.oakfusion.roots.external.mongo.readmodel.MongoReadModelStorage;
import pl.oakfusion.roots.external.mongo.storage.MongoDatabaseProvider;
import pl.oakfusion.roots.impl.mail.DefaultEmailManagerBuilder;
import pl.oakfusion.roots.impl.type.StrictTypeRegistry;
import pl.oakfusion.roots.web.config.RootsSpringConfiguration;
import pl.oakfusion.security.*;

import java.util.List;
import java.util.function.Supplier;

import static pl.oakfusion.idm.Config.EventName.*;
import static pl.oakfusion.roots.impl.mail.DefaultEmailManager.DefaultDecorators.ID_ASSIGNING_STRATEGY;

@Configuration
@Import({RootsSpringConfiguration.class})
@EnableConfigurationProperties({SecurityConstants.class})
@RefreshScope
public class Config {

	private static final String DEFAULT_TYPE_PREFIX = "pl.oakfusion.idm";
	private static final String ACTIVATE_ACCOUNT = "activate-account";
	private static final String CHANGE_PASSWORD = "change-password";
	private static final String GRANT_ROLE = "grant-role";
	private static final String REVOKE_ROLE = "revoke-role";

	public static final String REGISTER_ACCOUNT = "register-account";
	public static final String REGISTER_ACCOUNT_WITH_ROLE = "register-account-with-role";
	public static final String ACTIVATE_ACCOUNT_WITH_PASSWORD = "activate-account-with-password";
	public static final String AUTHENTICATE = "authenticate";
	public static final String LOGOUT = "logout";
	public static final String USER_ACCOUNT_PROJECTION = "user-account-data";
	public static final String USER_READ_MODEL = "user-read-model";

	@Bean
	@ConfigurationProperties(prefix="idm.roles")
	public Roles roles(){
		return new Roles();
	}

	private @Value("${mongo.readmodel.dbname}")
	String readModelDatabaseName;


	enum EventName {
		ACCOUNT_REGISTERED_WITH_PASSWORD,
		ACCOUNT_REGISTERED_WITH_ROLES,
		ACCOUNT_ACTIVATED,
		ACCOUNT_ACTIVATED_WITH_PASSWORD,
		AUTHENTICATED,
		AUTHENTICATION_FAILED,
		LOGGED_OUT,
		ACCOUNT_BLOCKED,
		PASSWORD_CHANGED,
		PASSWORD_CHANGE_FAILED,
		ACCOUNT_EXPIRED,
		ROLE_GRANTED,
		ROLE_REVOKED;
	}

	@Bean
	TypeRegistry typeRegistry() {
		return new StrictTypeRegistry()
				.setPrefix(DEFAULT_TYPE_PREFIX)
				.register(ACCOUNT_REGISTERED_WITH_PASSWORD, AccountRegisteredWithPassword.class)
				.register(ACCOUNT_REGISTERED_WITH_ROLES, AccountRegisteredWithRoles.class)
				.register(ACCOUNT_ACTIVATED, AccountActivated.class)
				.register(ACCOUNT_ACTIVATED_WITH_PASSWORD, AccountActivatedWithPassword.class)
				.register(AUTHENTICATED, Authenticated.class)
				.register(AUTHENTICATION_FAILED, AuthenticationFailed.class)
				.register(LOGGED_OUT, LoggedOut.class)
				.register(ACCOUNT_BLOCKED, AccountBlocked.class)
				.register(ACCOUNT_EXPIRED, AccountExpired.class)
				.register(PASSWORD_CHANGED, PasswordChanged.class)
				.register(PASSWORD_CHANGE_FAILED, PasswordChangeFailed.class)
				.register(USER_READ_MODEL, User.class)
				.register(ROLE_GRANTED, RoleGranted.class)
				.register(ROLE_REVOKED, RoleRevoked.class);
	}

	@Bean
	Context context(UserAggregate userAggregate, IdmCommandFactory idmCommandFactory) {
		return new Context()
				.featureInfo("Account management")
				.registerCommandMapping(REGISTER_ACCOUNT, RegisterAccountWithPassword.class, userAggregate)
				.registerCommandMapping(REGISTER_ACCOUNT_WITH_ROLE, RegisterAccountWithRole.class, userAggregate)
				.registerCommandMapping(ACTIVATE_ACCOUNT, ActivateAccount.class, userAggregate, idmCommandFactory::activateAccountByOwner)
				.registerCommandMapping(ACTIVATE_ACCOUNT_WITH_PASSWORD, ActivateAccountWithPassword.class, userAggregate, idmCommandFactory::activateAccountByOwnerWithPassword)
				.registerCommandMapping(AUTHENTICATE, Authenticate.class, userAggregate, idmCommandFactory::authenticate)
				.registerCommandMapping(LOGOUT, LogoutUser.class, userAggregate)
				.registerCommandMapping(CHANGE_PASSWORD, ChangePassword.class, userAggregate)
				.registerCommandMapping(GRANT_ROLE, GrantRole.class, userAggregate)
				.registerCommandMapping(REVOKE_ROLE, RevokeRole.class, userAggregate)
				.registerProjection(USER_ACCOUNT_PROJECTION);
	}

	@Bean
	MongoDatabaseProvider mongoDatabaseProvider(@Value("${mongo.uri}") String mongoUri) {
		return new MongoDatabaseProvider(mongoUri);
	}

	@Bean
	EventStorage eventStorage(@Value("${mongo.events.dbname}") String dbname,
							  MongoDatabaseProvider mongoDatabaseProvider,
							  ObjectMapper objectMapper,
							  TypeRegistry typeRegistry) {
		MongoDatabase database = mongoDatabaseProvider.getDatabase(dbname);
		return new MongoEventStorage(database, objectMapper, typeRegistry);
	}

	@Bean
	MongoFilterMapper mongoFilterMapper() {
		return new MongoFilterMapper();
	}

	@Bean
	ReadModelStorage readModelStorage(MongoDatabaseProvider mongoDatabaseProvider,
									  MongoFilterMapper filterMapper,
									  ObjectMapper objectMapper,
									  TypeRegistry typeRegistry) {
		MongoDatabase database = mongoDatabaseProvider.getDatabase(readModelDatabaseName);
		return new MongoReadModelStorage(database, filterMapper, objectMapper, typeRegistry);
	}

	@Bean
	EmailLookup emailLookup(MongoDatabaseProvider mongoDatabaseProvider) {
		MongoDatabase database = mongoDatabaseProvider.getDatabase(readModelDatabaseName);
		return new EmailLookup(database);
	}

	@Bean
	BlacklistJWTLookup blacklistJWTLookup(MongoDatabaseProvider mongoDatabaseProvider) {
		MongoDatabase database = mongoDatabaseProvider.getDatabase(readModelDatabaseName);
		return new BlacklistJWTLookup(database);
	}

	// ============

	@Bean
	PasswordEncryptor passwordEncryptor() {
		return new DefaultPasswordEncryptor();
	}

	@Bean
	AccountExpirationStrategy expirationStrategy() {
		return new NoExpirationStrategy();
	}

	@Bean
    TokenManager tokenManager(SecurityConstants securityConstants) {
		byte[] signingKey = securityConstants.getKey().getBytes();
		return new JWTManager(signingKey);
	}

	@Bean
	SecurityCheck securityCheck(BlacklistJWTLookup blacklistJWTLookup, SecurityConstants securityConstants) {
		return new DefaultSecurityCheck(blacklistJWTLookup, securityConstants);
	}

	@Bean
	UserAggregate userAggregate(@Value("${idm.activation-message-expiration-period-id-days:2}") long activationMessageExpirationPeriodIdDays,
								@Value("${idm.failed-authentication-threshold:3}") int failedAuthenticationThreshold,
								EmailLookup emailLookup, AccountExpirationStrategy expirationStrategy,
								EmailManager emailManager, PasswordEncryptor passwordEncryptor,
								SecurityConstants securityConstants,
								Roles roles,
								BlacklistJWTLookup blacklistJWTLookup,
								TokenManager tokenManager) {
		return new UserAggregate(
				activationMessageExpirationPeriodIdDays,
				failedAuthenticationThreshold,
				emailLookup,
				expirationStrategy,
				emailManager,
				passwordEncryptor,
				securityConstants,
				roles,
				blacklistJWTLookup,
                tokenManager
		);
	}

	@Bean
	UserAccountProjection accountProjection(MessageBus messageBus, ReadModelStorage readModelStorage) {
		return new UserAccountProjection(USER_ACCOUNT_PROJECTION, User::new, readModelStorage, messageBus);
	}

	@Bean
	IdmCommandFactory idmCommandFactory() {
		return new IdmCommandFactory();
	}

	@Bean
	Supplier<List<Class<? extends RuntimeException>>> commandProcessorRethrownExceptions() {
		return () -> ImmutableList.of(
				AuthenticationException.class
		);
	}

	@Bean
	AdminInit adminInit(CommandProcessor commandProcessor, Query query, Roles roles) {
		AdminInit adminInit = new AdminInit(commandProcessor, query, roles);
		adminInit.initAdmin();
		return adminInit;
	}

	@Configuration
	@Profile("dev")
	public static class DevConfig {

		@Bean
		EmailManager emailManager(@Value("${email.default_sender.address}") String senderAddress,
								  @Value("${email.default_sender.password}") String senderPassword,
								  @Value("${email.host.address}") String hostAddress,
								  @Value("${email.host.port}") int hostPort,
								  @Value("${email.templates-location}") String templatesLocation) {
			return new DefaultEmailManagerBuilder()
					.withSMTPServer(hostAddress, hostPort, senderAddress, senderPassword)
					.withTemplateLocation(templatesLocation)
					.withDecorator(ID_ASSIGNING_STRATEGY)
					.withDecorator(email -> email.setFrom(senderAddress))
					.withDecorator(email -> email.setReplyTo(senderAddress))
					.build();
		}
	}

	@Configuration
	@Profile("prod")
	public static class ProdConfig {

		@Bean
		EmailManager emailManager(@Value("${email.default_sender.address}") String senderAddress,
								  @Value("${email.default_sender.password}") String senderPassword,
								  @Value("${email.host.address}") String hostAddress,
								  @Value("${email.host.port}") int hostPort,
								  @Value("${email.templates-location}") String templatesLocation) {
			return new DefaultEmailManagerBuilder()
					.withSMTPServer(hostAddress, hostPort, senderAddress, senderPassword)
					.withTemplateLocation(templatesLocation)
					.withDecorator(ID_ASSIGNING_STRATEGY)
					.withDecorator(email -> email.setFrom(senderAddress))
					.withDecorator(email -> email.setReplyTo(senderAddress))
					.build();
		}
	}
}
