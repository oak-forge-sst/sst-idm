package pl.oakfusion.idm.exception;

public enum ErrorCode {

	INVALID_EMAIL_ADDRESS_FORMAT,
	EMAIL_AND_PASSWORD_ARE_BOTH_REQUIRED,
	PASSWORD_AND_PASSWORD_CONFIRMATION_ARE_BOTH_REQUIRED,
	PASSWORD_AND_PASSWORD_CONFIRMATION_DO_NOT_MATCH,
	NO_ROLE_PROVIDED,
	NO_PASSWORD_PROVIDED,

	EMAIL_ALREADY_TAKEN,
	NO_SUCH_EMAIL,
	ACTIVATION_MESSAGE_FAILURE,
	ACTIVATION_TOKEN_REQUIRED,
	ACTIVATION_TOKEN_EXPIRED,
	ACTIVATION_TOKEN_INVALID,

	INCORRECT_LOGIN_OR_PASSWORD,
	ACCOUNT_BLOCKED,
	ACCOUNT_EXPIRED,

	INVALID_EMAIL_ADDRESS,
	ROLE_ALREADY_GRANTED,
	ROLE_NOT_GRANTED

}
