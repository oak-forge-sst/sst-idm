package pl.oakfusion.idm.exception;

import pl.oakfusion.roots.api.exception.BusinessException;

public class AccountRegistrationException extends BusinessException {

	private static final long serialVersionUID = 1L;

	public AccountRegistrationException(Enum errorCode, String... params) {
		super(errorCode, params);
	}

	public AccountRegistrationException(Enum errorCode, Exception e, String... params) {
		super(errorCode, e, params);
	}
}
