package pl.oakfusion.idm.security;

import org.apache.http.impl.client.*;
import org.apache.http.ssl.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.cloud.config.client.*;
import org.springframework.context.annotation.*;
import org.springframework.core.io.*;
import org.springframework.http.client.*;
import org.springframework.web.client.*;

import javax.net.ssl.*;
import java.io.*;

@Configuration
public class SslConfig {

	public static final String KEY_STORE_FILE_PATH = "sst-idm.p12";

	@Autowired
	ConfigClientProperties properties;

	@Value("${key-store-password}")
	String propertiesPassword;

	@Primary
	@Bean
	public ConfigServicePropertySourceLocator configServicePropertySourceLocator() throws Exception {
		final char[] password = (propertiesPassword == null) ? System.getenv("SERVICE_BOX_KEYSTORE_PASS").toCharArray() : propertiesPassword.toCharArray();
		final File keyStoreFile = new ClassPathResource(KEY_STORE_FILE_PATH).getFile();

		SSLContext sslContext = SSLContexts.custom()
				.loadKeyMaterial(keyStoreFile, password, password)
				.loadTrustMaterial(keyStoreFile, password).build();
		CloseableHttpClient httpClient = HttpClients.custom().setSSLContext(sslContext).build();
		HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(httpClient);
		ConfigServicePropertySourceLocator configServicePropertySourceLocator = new ConfigServicePropertySourceLocator(properties);
		configServicePropertySourceLocator.setRestTemplate(new RestTemplate(requestFactory));
		return configServicePropertySourceLocator;
	}
}
