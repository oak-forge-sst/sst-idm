package pl.oakfusion.idm.security;

import org.jasypt.util.password.StrongPasswordEncryptor;

public class DefaultPasswordEncryptor implements PasswordEncryptor {

	private final StrongPasswordEncryptor encryptor;

	public DefaultPasswordEncryptor() {
		this.encryptor = new StrongPasswordEncryptor();
	}

	@Override
	public String encryptPassword(String password) {
		return encryptor.encryptPassword(password);
	}

	@Override
	public boolean checkPassword(String inputPassword, String encryptedPassword) {
		return encryptor.checkPassword(inputPassword, encryptedPassword);
	}
}
