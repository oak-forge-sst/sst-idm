package pl.oakfusion.idm.security;

import io.jsonwebtoken.*;
import io.jsonwebtoken.security.Keys;
import pl.oakfusion.security.*;

import java.util.*;

public class JWTManager implements TokenManager {

    private final String TOKEN_TYPE = "JWT";
    private final byte[] signingKey;

    public JWTManager(byte[] signingKey) {
        this.signingKey = signingKey;
    }

    public String createToken(String emailAddress, List<String> roles) {
        return Jwts.builder()
                .signWith(Keys.hmacShaKeyFor(signingKey), SignatureAlgorithm.HS512)
                .setHeaderParam("typ", TOKEN_TYPE)
                .setSubject(emailAddress)
                .setExpiration(new Date(System.currentTimeMillis() + 864000000))
                .claim("rol", roles)
                .compact();
    }

    @Override
    public String getEmailFromToken(String token) {
        Jws<Claims> parsedToken = Jwts.parser()
                .setSigningKey(signingKey)
                .parseClaimsJws(token);

        return parsedToken
                .getBody()
                .getSubject();
    }
}
