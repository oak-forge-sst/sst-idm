package pl.oakfusion.idm.security;

import com.mongodb.client.MongoDatabase;
import pl.oakfusion.roots.external.mongo.storage.MongoSimpleKeyValueStorage;

import java.util.UUID;

public class BlacklistJWTLookup extends MongoSimpleKeyValueStorage<String, UUID> {

	private static final String BLACKLIST_JWT_LOOKUP = "blacklist-jwt-lookup";

	public BlacklistJWTLookup(MongoDatabase mongoDatabase) {
		super(mongoDatabase, BLACKLIST_JWT_LOOKUP);
	}
}
