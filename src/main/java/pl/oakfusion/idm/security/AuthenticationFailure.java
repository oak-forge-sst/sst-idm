package pl.oakfusion.idm.security;

import pl.oakfusion.roots.api.event.trait.Failure;

public interface AuthenticationFailure extends Failure {

	default void fail(String message) {
		throw new AuthenticationException(message);
	}
}
