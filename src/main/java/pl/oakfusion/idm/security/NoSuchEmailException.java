package pl.oakfusion.idm.security;

import pl.oakfusion.idm.exception.ErrorCode;

public class NoSuchEmailException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	NoSuchEmailException(String message) {
		super(message);
	}

	public static NoSuchEmailException emailException(ErrorCode code) {
		return new NoSuchEmailException(code.name());
	}
}
