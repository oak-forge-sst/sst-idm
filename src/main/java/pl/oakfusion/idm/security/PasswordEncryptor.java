package pl.oakfusion.idm.security;

public interface PasswordEncryptor {

	String encryptPassword(String password);

	boolean checkPassword(String inputPassword, String encryptedPassword);
}
