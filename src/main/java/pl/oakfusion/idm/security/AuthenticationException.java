package pl.oakfusion.idm.security;

import pl.oakfusion.idm.exception.ErrorCode;

public class AuthenticationException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	AuthenticationException(String message) {
		super(message);
	}

	public static AuthenticationException authenticationException(ErrorCode code) {
		return new AuthenticationException(code.name());
	}

}
