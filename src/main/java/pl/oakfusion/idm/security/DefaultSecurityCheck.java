package pl.oakfusion.idm.security;

import io.jsonwebtoken.*;
import pl.oakfusion.security.SecurityCheck;

public class DefaultSecurityCheck implements SecurityCheck {

    private final BlacklistJWTLookup blacklistJWTLookup;
    private final SecurityConstants securityConstants;

    public DefaultSecurityCheck(BlacklistJWTLookup blacklistJWTLookup, SecurityConstants securityConstants) {
        this.blacklistJWTLookup = blacklistJWTLookup;
        this.securityConstants = securityConstants;
    }

    @Override
    public boolean isAuthenticated(String jwt) {
        boolean isBlacklisted = blacklistJWTLookup.get(jwt).isPresent();
        byte[] signingKey = securityConstants.getKey().getBytes();

        if (isBlacklisted) return false;

        try {
            Jwts.parser()
                    .setSigningKey(signingKey)
                    .parseClaimsJws(jwt);
        } catch (JwtException e) {
            return false;
        }
        return true;
    }
}
