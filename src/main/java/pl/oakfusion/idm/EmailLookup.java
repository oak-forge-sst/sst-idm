package pl.oakfusion.idm;

import com.mongodb.client.MongoDatabase;
import pl.oakfusion.roots.external.mongo.storage.MongoSimpleKeyValueStorage;

import java.util.UUID;

public class EmailLookup extends MongoSimpleKeyValueStorage<String, UUID> {

	private static final String EMAIL_LOOKUP = "email-lookup";

	EmailLookup(MongoDatabase mongoDatabase) {
		super(mongoDatabase, EMAIL_LOOKUP);
	}
}
