package pl.oakfusion.idm_commands;

import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

import java.util.*;

public class IdmSecurityEndpointsHandler {

    public static List<RequestMatcher> getPermitAllIdmMatches() {
        return Arrays.asList(
                new IdmCommandMatcher(IdmCommands.ACTIVATE_ACCOUNT, IdmCommands.REGISTER_ACCOUNT),
                new AntPathRequestMatcher("/idm/login")
        );
    }

    public static List<RequestMatcher> getAdminRolesIdmMatches() {
        return Arrays.asList(new IdmCommandMatcher(IdmCommands.GRANT_ROLE,
                IdmCommands.REVOKE_ROLE,
                IdmCommands.REGISTER_ACCOUNT_WITH_ROLE
        ));
    }
}
