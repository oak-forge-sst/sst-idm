package pl.oakfusion.idm_commands;

public class IdmCommands {
    public static final String REGISTER_ACCOUNT = "register-account";
    public static final String REGISTER_ACCOUNT_WITH_ROLE = "register-account-with-role";
    public static final String ACTIVATE_ACCOUNT = "activate-account";
    public static final String ACTIVATE_ACCOUNT_WITH_PASSWORD = "activate-account-with-password";
    public static final String AUTHENTICATE = "authenticate";
    public static final String CHANGE_PASSWORD = "change-password";
    public static final String GRANT_ROLE = "grant-role";
    public static final String REVOKE_ROLE = "revoke-role";
    public static final String USER_ACCOUNT_PROJECTION = "user-account-data";
    public static final String USER_READ_MODEL = "user-read-model";

}
