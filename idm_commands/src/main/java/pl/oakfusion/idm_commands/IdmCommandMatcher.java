package pl.oakfusion.idm_commands;

import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class IdmCommandMatcher implements RequestMatcher {
    private final static String PATH_TO_COMMAND = "/idm/command";
    private final static String COMMAND = "command";
    private final List<String> commands;

    IdmCommandMatcher(String... command) {
        this.commands = Arrays.stream(command).collect(Collectors.toList());
    }

    @Override
    public boolean matches(HttpServletRequest request) {
        String headerParam = request.getHeader(COMMAND);

        return commands.stream().anyMatch(command ->
                (new AntPathRequestMatcher(PATH_TO_COMMAND).matches(request) && headerParam != null && headerParam.equals(command)) ||
                        new AntPathRequestMatcher(PATH_TO_COMMAND + command).matches(request));
    }
}
