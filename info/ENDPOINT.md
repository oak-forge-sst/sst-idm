#Endpoints

| HTTP method | Endpoint | Details | Requirements | Return data |
| :---: | :---: | :---: | :---: | :---: |
| POST  | /idm/command | Process various commands |- header <br> - JSON body | JSON which may contain: <br> - aggregateId <br> - failure <br> - values <br> - or just error message  |
| POST  | /idm/login | Sign in User | - JSON body | JSON which may contain: <br> - aggregateId <br> - failure <br> - values <br> - or just error message |
| POST  | /idm/logout | Sign out User | - cookie with idmToken | JSON which may contain: <br> - aggregateId <br> - failure <br> - values <br> - or just error message |
<br>

##Command endpoints (/idm/command) - use cases

| Headers | Body | Details | Return data (success) | Return data (failure) |
| :---: | :---: | :---: | :---: | :---: |
| key: command <br> value: register-account  | ![JSONAddUser](images/JSONAddUser.png) | Add new user | - empty JSON and, <br> - email with "token" | - JSON with message error |
| key: command <br> value: activate-account  | ![JSONActivateUser](images/JSONActivateUser.png) | Activate User | - empty JSON | - JSON with message error |
<br>

##Other endpoints - use cases

| Endpoint | Cookie | Body | Details | Return data (success) | Return data (failure) |
| :---: | :---: | :---: | :---: | :---: | :---: |
| /idm/login | ---------- | ![JSONLogin](images/JSONLogin.png) | Sign in user | - JSON with User unique ID and, <br> - idmToken cookie with token | - JSON with message error |
| /idm/logout | name: idmToken <br> value: token  | ---------- | Sign out user | - JSON with User unique ID | - JSON with message error |

